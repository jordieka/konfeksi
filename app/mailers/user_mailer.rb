class UserMailer < ApplicationMailer
  default from: 'Admin Konfeksi Marken'

  def send_welcome_email(user)
    @user = user
    mail(:to => @user.email, :subject => "Welcome!")
  end

  def forgot_password(user)
  	@user = user
  	mail(:to => @user.email, :subject => "Forgot Password")
  end

  def new_user(user)
  	@user = user
    @url  = ENV["url"].to_s + "/login"
    mail(:to => @user.email, :subject => "Welcome to Konfeksi Marken!")
  end
end
