class ApplicationMailer < ActionMailer::Base
  default from: 'Admin Konfeksi Marken'
  layout 'mailer'
end
