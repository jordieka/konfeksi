class MaterialMailer < ApplicationMailer
  	default from: 'Sistem Konfeksi Marken'

	def request_material(request)

		@request = request
		@user = User.active.find_by(role: "pengawas_konfeksi")

		mail(:to => "jortheeka25mu@gmail.com", :subject => "New Material Request")
	end

	def update_request_material(request)

		@request = request
		@user = User.active.find_by(role: "pengawas_produksi")

		mail(:to => "jortheeka25mu@gmail.com", :subject => "Material Request Update for Order - #{@request.order.invoice}")
	end

end
