class OrderMailer < ApplicationMailer
	default from: 'Sistem Konfeksi Marken'

	def order(request)

		@request = request
		@user = User.active.find_by(role: "pengawas_konfeksi")

		mail(:to => "jortheeka25mu@gmail.com", :subject => "New Material Request")
	end

	def self.order_ready(order)
		users = User.where(role: [:sekretaris, :owner])
		users.each do |user|
			send_order_ready(order, user).deliver_now			
		end

		send_order_ready(order, order.customer).deliver_now if order.customer.email.present?
	end

	def send_order_ready(order, user)
		@user = user
		@order = order
		mail(:to => "jortheeka25mu@gmail.com", :subject => "Order - #{@order.invoice} are on production!")
	end
end
