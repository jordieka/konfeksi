// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require jquery3
//= require jquery_ujs
//= require toastr
//= require popper
//= require jquery.nicescroll
//= require bootstrap-sprockets
//= require stisla
//= require scripts
//= require custom
//= require_tree .

// Restricts input for the given textbox to the given inputFilter.
(function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  };
}(jQuery));

// Install input filters.
$(document).ready(function() {
  $("#only-numbers").inputFilter(function(value) {
    return /^\d*$/.test(value);    // Allow digits only, using a RegExp
  });
  $(".only-numbers").inputFilter(function(value) {
    return /^\d*$/.test(value);    // Allow digits only, using a RegExp
  });
});

$(document).ready(function() {
  $('.select2').select2({
    closeOnSelect: true
  });
});

function formatted_date(m){
  var dateString =
    ("0" + m.getUTCDate()).slice(-2) + "-" +
    ("0" + (m.getUTCMonth()+1)).slice(-2) + "-" +
    m.getUTCFullYear() + " " +
    ("0" + m.getUTCHours()).slice(-2) + ":" +
    ("0" + m.getUTCMinutes()).slice(-2);

  return dateString
}

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}

var selected = []
var data = {}

function update_value_items(){
  var value = JSON.stringify(data);
  console.log(data);
  console.log(value);
  $('#items').val(window.btoa(value));
}

function delete_row(param) {
   row = $("#table-item tr").length < 1

   $("#material-"+param).remove();
   var index = selected.indexOf(param);

    if (index > -1) {
       selected.splice(index, 1);
    }

    if (row == true) {
      $("#table-item").append("<tr class='no-item-td text-center'><td colspan='4'><b>No Item</b></td></tr>");
    }

    update_value_items()

   console.log("selected");
   console.log("yes : ", index);
   console.log("yes : ", row);
}

$(document).ready(function(){
  
  if($('#items').length > 0){
    items_values = jQuery.parseJSON($('#items').val());
    console.log(items_values);
    if (items_values) {
      data = items_values;
      selected.push(Object.keys(items_values));
      $('#items').val(window.btoa(JSON.stringify(data)));
    }
    console.log($('#items').val());
  }

  $("#btn-add-items").click(function (e) {
    e.preventDefault();
    $(".no-item-td").remove();
    var last = $("#table-item tr").length; //think about it ;)
    var id = 1 + last - 1;

    var material_value = $("#material-option").val();
    var qnt = parseFloat($(".qnt").val());

    if (isNaN(qnt)) {
      alert("Harap masukan kuantiti terlebih dahulu!");
    }else{

      if (selected.length > 0 && selected.includes(material_value)) {
        $("#qnt-"+material_value).text(qnt);
        data[material_value] = qnt
      }else{
        selected.push(material_value);
        data[material_value] = qnt
        console.log(data);
        var material = $( "#material-option option:selected" ).text();
        $("#table-item").append("<tr id='material-"+material_value+"'><td>"+id+"</td><td>"+material+"</td><td id='qnt-"+material_value+"'>"+qnt+"</td><td><button class='btn btn-danger btn-lg' onclick='delete_row("+material_value+")'><i class='fas fa-times delete-material'> </i></button></td></tr>");
      }

    }

    update_value_items()
  });

  $("#submit-aja").click(function (e) {
    console.log(selected);
    console.log(data);
  });

});

function calc_qnt(){
  var sum = 0;

  $('.buyed-qnt').each(function(){
    qnt = $(this).val();
    if (!qnt) {
      qnt = 0;
    }
    sum += parseFloat(qnt); 
  });

  $('#qnt-buyed').text(sum);
}


function calc_price(){
  var total = 0;

  $('.buyed-price').each(function(){
    sub = $(this).val();
    if (!sub) {
      sub = 0;
    }
    total += parseFloat(sub); 
  });

  $('#price-buyed').text("Rp. " + addCommas(total));
}


$(window).on('load', function() { 
  calc_qnt();
  calc_price();
});

$(document).ready(function(){

  $(".buyed-qnt").keyup(function(){
    qnt = $(this).val();
    calc_qnt(qnt);
  }); 

  $(".buyed-price").keyup(function(){
    sub = $(this).val();
    calc_price(sub);
  });   

  $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  });

});

$(document).ready(function() {

  $("#show-bill-image").on("click", function(){
    $("#img01").attr('src', "");
    if ($("#showBillImage").length == 0) {
      alert("Image not found.")
    }else{
      $("#myModal").css({"display": "block"});
      $("#img01").attr('src', ($("#showBillImage").attr('src')));
    }
  });

  $("#show-payment-image").on("click", function(){
    $("#img01").attr('src', "");
    if ($("#showPaymentImage").length == 0) {
      alert("Image not found.")
    }else{
      $("#myModal").css({"display": "block"});
      $("#img01").attr('src', ($("#showPaymentImage").attr('src')));
    }
  });

  // When the user clicks on <span> (x), close the modal
  $(".close").on("click", function(){
    $("#myModal").css({"display": "none"});
  });

  $('#updateStatusTransaction').on('show.bs.modal', function (event) {
    var statuses = ["Pending", "Payment Pending", "Paid", "Done"]
    var button = $(event.relatedTarget) // Button that triggered the modal
    var inv = button.data('whatever') // Extract info from data-* attributes
    var status = jQuery.inArray( button.data('status'), statuses ) // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    console.log(button.data('status'));
    var modal = $(this);
    modal.find('.modal-title').text('Update Status');
    $("#invoice").val(inv);
    $("#status").val(statuses[status + 1]);
    if (status != 1) {
      $("#pending-upload").html("");
    }
    $("#upload").val(inv);
  })
});

// update category

$(document).ready(function() {

  $('#updateCategory').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var name = button.data('whatever') // Extract info from data-* attributes
    var category = button.data('whenever') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.

    var modal = $(this);
    $("#categoryName").val(name);
    $("#categoryButton").val(category);
    modal.find('.modal-title').text('Update Category');
  })
});

// $('input[type=text]').each(function(){
//     selected.push($(this).val());
// })

function update_size_items(){
  var value = JSON.stringify(selected);
  $('#sizes').val(window.btoa(value));
}

function delete_size(param) {
   row = $("#table-item tr").length < 1
   console.log("row : " + row);
   console.log("param : " + param);
   $("#size-"+param).remove();
   var index = selected.indexOf(param);

    if (index > -1) {
       selected.splice(index, 1);
    }

    if (row == true) {
      $("#table-item").append("<tr class='no-item-td text-center'><td colspan='3'><b>No Item</b></td></tr>");
    }

    update_size_items()

   console.log(selected);
}


$(document).ready(function(){

  $("#btn-add-sizes").click(function (e) {
    // alert("kampang");
    e.preventDefault();
    $(".no-item-td").remove();
    var last = $("#table-item tr").length; //think about it ;)
    var id = 1 + last - 1;

    console.log(last);
    console.log(id);

    var size_value = $("#sizes-option").val();

    if (!selected.includes(size_value)) {
      selected.push(size_value);
      var size = $( "#sizes-option option:selected" ).text();
      var input = "<input type='text' name='margins[]' class='form-control only-numbers' required='required'>"
      $("#table-item").append("<tr class='text-center' id='size-"+size_value+"'><td>"+id+"</td><td>"+size+"</td><td>"+input+"</td><td><button class='btn btn-danger btn-lg' onclick='delete_size(\""+size_value+"\")'><i class='fas fa-times delete-material'> </i></button></td></tr>");
    }

    update_size_items()
  });

  $("#submit-aja").click(function (e) {
    console.log(selected);
    console.log(data);
  });

});

function update_material_items(){
  var value = JSON.stringify(data);
  console.log(data);
  console.log(value);
  $('#items').val(window.btoa(value));
}

function delete_material(param) {
   row = $("#table-item tr").length < 1
   $("#material-"+param).remove();
   var index = selected.indexOf(param);

    if (index > -1) {
       selected.splice(index, 1);
       delete data[param];
    }

    if (row == true) {
      $("#table-item").append("<tr class='no-item-td text-center'><td colspan='4'><b>No Item</b></td></tr>");
    }

    update_material_items()

   console.log(selected);
}

$(document).ready(function(){

  $("#btn-add-materials").click(function (e) {
    // alert("kampang");
    e.preventDefault();
    $(".no-item-td").remove();
    var last = $("#table-item tr").length; //think about it ;)
    var id = 1 + last - 1;

    var material_value = parseInt($("#material-option").val());
    var qnt = parseInt($(".qnt").val());

    if (isNaN(qnt)) {
      alert("Harap masukan kuantiti terlebih dahulu!");
    }else{

      if (selected.length > 0 && selected.includes(material_value)) {
        $("#qnt-"+material_value).text(qnt);
        data[material_value] = qnt
        $("#qnt-"+material_value).val(qnt);
        console.log("masuk");
      }else{
        selected.push(material_value);
        data[material_value] = qnt
        console.log(data);
        var material = $( "#material-option option:selected" ).text();
        $("#table-item").append("<tr id='material-"+material_value+"'><td>"+id+"</td><td>"+material+"</td><td id='qnt-"+material_value+"'>"+qnt+"</td><td><button class='btn btn-danger btn-lg' onclick='delete_material("+material_value+")'><i class='fas fa-times delete-material'> </i></button></td></tr>");
      }

    }

    update_material_items()
  });

  $("#submit-aja").click(function (e) {
    console.log(selected);
    console.log(data);
  });

});

function update_order_items(){
  var value = JSON.stringify(data);
  console.log(data);
  console.log(value);
  $('#orders').val(window.btoa(value));
}

function delete_row_order(param) {
   $("#order-"+param).remove();
   row = $("#table-item tr").length
   var index = selected.indexOf(String(param));

    if (index > -1) {
       selected.splice(index, 1);
    }

    if (row == 2 && $("#totalSummary").length) {
      $("#totalSummary").remove();
      $("#table-item").append("<tr class='no-item-td text-center'><td colspan='6'><b>No Item</b></td></tr>");
    }

    update_order_items()

   console.log(selected);
}

function update_negotiation_items(){
  var value = JSON.stringify(data);
  console.log(data);
  console.log(value);
  $('#orders').val(window.btoa(value));
}

function delete_row_negotiation(param) {
   $("#order-"+param).remove();
   row = $("#table-item tr").length
   var index = selected.indexOf(String(param));

    if (index > -1) {
       selected.splice(index, 1);
    }

    if (row == 2 && $("#totalSummary").length) {
      $("#totalSummary").remove();
      $("#table-item").append("<tr class='no-item-td text-center'><td colspan='6'><b>No Item</b></td></tr>");
    }

    update_order_items()

   console.log(selected);
}


var size_data = { xs: 0, s: 0, m: 0, l: 0, xl: 0, xxl: 0, xxxl: 0 };

function getKeyByValue(object, value) {
  return Object.keys(object).find(key => object[key] === value);
}

$(document).ready(function(){

  (function($) {
      $.fn.currencyFormat = function() {
          this.each( function( i ) {
              $(this).change( function( e ){
                  if( isNaN( parseFloat( this.value ) ) ) return;
                  this.value = parseFloat(this.value).toFixed(2);
              });
          });
          return this; //for chaining
      }
  })( jQuery );

  var base_price = $("#basePrice").val();
  var margin_price = $("#marginPrice").val();

  $("#productSelect").on("change", function(event){
    console.log($(this).val());

    $.ajax({
      type: 'GET',
      url: '/web_admin/products/get_product/'+$(this).val(),
      dataType: 'JSON',
      success: function(data){
        base_price = data["base_price"];
        $("#basePrice").val(base_price);
      }
    });

    $.ajax({
      type: 'GET',
      url: '/web_admin/products/get_size/'+$(this).val(),
      dataType: 'JSON',
      success: function(data){ 
        size_data = {}
        $("#sizesSelect").html("");

        $.each( data, function( key, value ) {
          size_data[value["id"]] = value["margin_price"];
          $("#sizesSelect").append("<option value='"+value["id"]+"'>"+value["size"]+"</option>");          
        });

        var id = String($("#sizesSelect").val());
        var total = parseInt(base_price) + parseInt(size_data[id]);

        $("#marginPrice").val(size_data[id]);
        $("#totalPrice").val(total);

        console.log(data);
        console.log(size_data);
      }
    });

  });

  $("#sizesSelect").on("change", function(event){
    console.log($("#productSelect").val());

    $.ajax({
      type: 'GET',
      url: '/web_admin/products/get_size/'+$("#productSelect").val(),
      dataType: 'JSON',
      success: function(data){     
        $.each( data, function( key, value ) {
          size_data[value["id"]] = value["margin_price"];
        });

        var id = String($("#sizesSelect").val());
        var total = parseInt(base_price) + parseInt(size_data[id]);

        $("#marginPrice").val(size_data[id]);
        $("#totalPrice").val(total);
      }
    });

    // console.log(total);
    // console.log(size_data);
  });


  $("#btn-add-order").click(function (e) {
    // alert("kampang");
      e.preventDefault();
      $(".no-item-td").remove();

      var product = $("#productSelect option:selected").text();
      var size =  $("#sizesSelect").val();
      var size_text =  $("#sizesSelect option:selected").text();
      var qnt = parseInt($("#quantity").val());
      var or_price = (parseFloat($("#totalPrice").val()));
      var price = (parseFloat($("#totalPrice").val()) * qnt);

      if (isNaN(qnt) || isNaN(price)) {
        alert("Kuantiti atau price tidak boleh kosong.");
      }else{

        if ($("#totalSummary").length) {
          $("#totalSummary").remove();
        }

        var last = $("#table-item tr").length; //think about it ;)
        var id = 1 + last - 1;

        if (selected.length > 0 && selected.includes(size)) {
          $("#qnt-"+size).text(qnt);
          new_price = "Rp " + addCommas(price)
          $("#price-"+size).text(new_price);
          data[size] = {qnt: qnt, price: or_price}
        }else{
          selected.push(size)
          data[size] = {qnt: qnt, price: or_price}
          price = addCommas(price)
          $("#table-item").append("<tr class='text-center' id='order-"+size+"'><td>"+id+"</td><td>"+product+"</td><td>"+size_text+"</td><td id='qnt-"+size+"'>"+qnt+"</td><td id='price-"+size+"'>Rp "+price+"</td><td><button class='btn btn-danger btn-lg' onclick='delete_row_order("+size+")'><i class='fas fa-times delete-material'> </i></button></td></tr>");         
        }

        qnt_total = 0
        price_total = 0
        $.each( data, function( key, value ) {
          qnt_total = qnt_total + parseInt(value["qnt"]) 
          price_total = price_total + (parseFloat(value["price"]) * parseInt(value["qnt"])) 
        });

        price_total = addCommas(price_total)

        if (selected.length > 0) {
          $("#table-item").append("<tr class='text-center' id='totalSummary'><td><h5>Total</h5></td><td colspan='2' id='totalKind'><h5>"+selected.length+" kind(s)</h5></td><td id='totalQnt'><h5>"+qnt_total+"</h5></td><td colspan='2' id='totalPrice'><h5>Rp "+price_total+"</h5></td></tr>");
        }

      }

      update_order_items()
    });

    $("#btn-add-negotiation").click(function (e) {
    // alert("kampang");
      e.preventDefault();
      $(".no-item-td").remove();

      var product = $("#productSelect option:selected").text();
      var size =  $("#sizesSelect").val();
      var size_text =  $("#sizesSelect option:selected").text();
      var qnt = parseInt($("#quantity").val());
      var or_price = (parseFloat($("#totalPrice").val()));
      var sablon_price = (parseFloat($("#printPrice").val()));
      var bordir_price = (parseFloat($("#embroideryPrice").val()));
      var price = ((parseFloat($("#totalPrice").val()) + parseFloat($("#printPrice").val()) + parseFloat($("#embroideryPrice").val()))* qnt);

      if (isNaN(qnt) || isNaN(price)) {
        alert("Kuantiti atau price tidak boleh kosong.");
      }else{

        if ($("#totalSummary").length) {
          $("#totalSummary").remove();
        }

        var last = $("#table-item tr").length; //think about it ;)
        var id = 1 + last - 1;

        if (selected.length > 0 && selected.includes(size)) {
          $("#qnt-"+size).text(qnt);
          new_price = "Rp " + addCommas(price)
          $("#price-"+size).text(new_price);
          data[size] = {qnt: qnt, price: or_price, bordir_price: bordir_price, sablon_price: sablon_price }
        }else{
          selected.push(size)
          data[size] = {qnt: qnt, price: or_price, bordir_price: bordir_price, sablon_price: sablon_price}
          price = addCommas(price)
          $("#table-item").append("<tr class='text-center' id='order-"+size+"'><td>"+id+"</td><td>"+product+"</td><td>"+size_text+"</td><td id='qnt-"+size+"'>"+qnt+"</td><td id='price-"+size+"'>Rp "+price+"</td><td><button class='btn btn-danger btn-lg' onclick='delete_row_negotiation("+size+")'><i class='fas fa-times delete-material'> </i></button></td></tr>");         
        }

        var qnt_total = 0
        var price_total = 0
        $.each( data, function( key, value ) {
          qnt_total = qnt_total + parseInt(value["qnt"]) 
          price_total = price_total + ((parseFloat(value["price"]) + parseFloat(value["bordir_price"] + parseFloat(value["sablon_price"]))) * parseInt(value["qnt"])) 
        });

        price_total = addCommas(price_total)

        $("#total_all").val(price_total);

        if (selected.length > 0) {
          $("#table-item").append("<tr class='text-center' id='totalSummary'><td><h5>Total</h5></td><td colspan='2' id='totalKind'><h5>"+selected.length+" kind(s)</h5></td><td id='totalQnt'><h5>"+qnt_total+"</h5></td><td colspan='2' id='totalPrice'><h5>Rp "+price_total+"</h5></td></tr>");
        }

      }

      update_negotiation_items()
    });

    $("#submit-aja").click(function (e) {
      console.log(selected);
      console.log(data);
    });

});



$(document).ready(function(){
  $(".show-pattern-image").on("click", function(){
    var id = this.value;;

    $("#img01").attr('src', "");
    $("#myModal").css({"display": "block"});
    $("#img01").attr('src', ($("#show-pattern-image-"+id).attr('src')));
  });

  if ($("#sizes").length > 0){
    sizes = $("#sizes").val();

    $('#sizes').val(window.btoa(sizes));
  }

  // When the user clicks on <span> (x), close the modal
  $(".close").on("click", function(){
    $("#modalOrderItems").css({"display": "none"});
  });

  $('#modalOrderItems').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var inv = button.data('whatever')
    console.log(inv);
    $.ajax({
      type: 'GET',
      url: '/web_admin/orders/order_items/'+inv,
      dataType: 'JSON',
      success: function(data){
        console.log(data);
        $("#orderItems").html("");
        $("#orderItems").append("<tr><th>#</th><th>Product</th><th>Status</th><th>Quantity</th><th width='200px'>Subtotal</th></tr>");
        $.each( data, function( key, value ) {
          string = "<tr><td>"+(key + 1)+"</td><td>"+value["product_name"]+"</td><td>"+capitalizeFirstLetter(value["production_status"])+"</td><td>"+value["quantity"]+"</td><td>"+value["sub_total"]+"</td></tr>"
          $("#orderItems").append(string);          
        });
      }
    });
  })

});

$(document).ready(function(){


  if ($("#productSize").length > 0) {
    function get_order_items(val){
      $.ajax({
        type: 'GET',
        url: '/web_admin/productions/get_sizes/'+val,
        dataType: 'JSON',
        success: function(data){ 
          size_data = {}
          $("#productSize").html("");
  
          $.each( data["items"], function( key, value ) {
            size_data[value["id"]] = value["size"];
            $("#productSize").append("<option value='"+value["id"]+"'>"+value["size"]+"</option>");          
          });
  
          console.log(data["items"]);
          console.log(size_data);
          console.log(data);
          $("#quantity").val(data["qnt"]);
        }
      });
    }
  
    get_order_items($("#orderItem").val());
  
    $("#orderItem").on("change", function(event){
      console.log($(this).val());
  
      get_order_items($(this).val());
  
    });
  
    $("#productSize").on("change", function(event){
      $.ajax({
        type: 'GET',
        url: '/web_admin/productions/show_size/'+$(this).val(),
        dataType: 'JSON',
        success: function(data){     
  
        }
      });
    });
  }

});


$(document).ready(function(){

  $('#modalLogs').on('show.bs.modal', function (event) {
    $.ajax({
      type: 'GET',
      url: '/web_admin/productions/show_logs/'+window.location.pathname.split("/").pop(),
      dataType: 'JSON',
      success: function(data){
        $("#productionLogs").html("");
        $("#productionLogs").append("<tr><th>#</th><th>Product</th><th>Timestamp</th><th>Status</th><th>Quantity Finish</th></tr>");
        $.each( data, function( key, value ) {
          created =  new Date(value["created_at"]);
          new_date = formatted_date(created);
          console.log(data);
          console.log(value["product"]);
          string = "<tr><td>"+(key + 1)+"</td><td>"+value["product"]+"</td><td>"+new_date+"</td><td>"+capitalizeFirstLetter(value["status"])+"</td><td>"+value["quantity_finish"]+"</td></tr>"
          $("#productionLogs").append(string);          
        });
      }
    });
  });

  $(".show-log-image").on("click", function(){
    id = $(this).val();
    console.log(id);
    $("#img01").attr('src', "");
    if ($("#show-log-image-"+id).length == 0) {
      alert("Image not found.")
    }else{
      $("#myModal").css({"display": "block"});
      $("#img01").attr('src', ($("#show-log-image-"+id).attr('src')));
    }
  });

  // When the user clicks on <span> (x), close the modal
  $(".close").on("click", function(){
    $("#myModal").css({"display": "none"});
  });

  $('#updateOrderTransaction').on('show.bs.modal', function (event) {
    var statuses = ["FinishProduction", "Paid", "Sending", "Success"]
    var button = $(event.relatedTarget) // Button that triggered the modal
    var inv = button.data('invoice') // Extract info from data-* attributes
    var status = jQuery.inArray( button.data('status'), statuses ) // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    console.log(button.data('status'));
    var modal = $(this);
    modal.find('.modal-title').text('Update Status');
    $("#invoice").val(inv);
    $("#status").val(statuses[status + 1]);
    if (status == 2) {
      $("#payment-upload").html("");
    }
    if (status != 1) {
      $("#kurir").html("");
    }
    if (status != 1) {
      $("#shipping_invoice").html("");
    }
    $("#upload").val(inv);
  })

  $("#pattern-image-button").on("click", function(){
    console.log($("#patternImage").attr('src'));

    $("#img01").attr('src', "");
    if ($("#patternImage").length == 0) {
      alert("Image not found.")
    }else{
      $("#myModal").css({"display": "block"});
      $("#img01").attr('src', ($("#patternImage").attr('src')));
    }
  });

  $("#print-image-button").on("click", function(){
    console.log($("#printImage").attr('src'));

    $("#img01").attr('src', "");
    if ($("#printImage").length == 0) {
      alert("Image not found.")
    }else{
      $("#myModal").css({"display": "block"});
      $("#img01").attr('src', ($("#printImage").attr('src')));
    }
  });

  $("#embroidery-image-button").on("click", function(){
    console.log($("#embroideryImage").attr('src'));

    $("#img01").attr('src', "");
    if ($("#embroideryImage").length == 0) {
      alert("Image not found.")
    }else{
      $("#myModal").css({"display": "block"});
      $("#img01").attr('src', ($("#embroideryImage").attr('src')));
    }
  });

  if ($("#pembayaran").val() == "lunas") {
      $("#downPayment").attr("disabled",true);
      $("#downPayment").attr("request",false);
  }else{
    $("#downPayment").attr("disabled",false);
  }

  $("#pembayaran").on("change", function(){
    if ($(this).val() == "lunas") {
      $("#downPayment").attr("disabled",true);
      $("#downPayment").attr("request",false);
    }else{
      $("#downPayment").attr("disabled",false);
    }
  });
});

function addURL(element)
{
    $(element).attr('href', function() {
        var year = $("#filter_year").val();
        var month = $("#filter_month").val();

        params = "?filter[year]=" + year + "&filter[month]=" + month;

        return this.href + params;
    });
}



$(document).ready( function () {
    $('#dataTables').DataTable();
} );