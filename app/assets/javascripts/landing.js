//= require jquery3
//= require jquery_ujs
//= require bootstrap-sprockets
//= require vendor/jquery.easing

(function($) {
  "use strict"; // Start of use strict

  // Closes the sidebar menu
  $(".menu-toggle").click(function(e) {
    e.preventDefault();
    $("#sidebar-wrapper").toggleClass("active");
    $(".menu-toggle > .fa-bars, .menu-toggle > .fa-times").toggleClass("fa-bars fa-times");
    $(this).toggleClass("active");
  });

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('#sidebar-wrapper .js-scroll-trigger').click(function() {
    $("#sidebar-wrapper").removeClass("active");
    $(".menu-toggle").removeClass("active");
    $(".menu-toggle > .fa-bars, .menu-toggle > .fa-times").toggleClass("fa-bars fa-times");
  });

  // Scroll to top button appear
  $(document).scroll(function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });

  console.log("testing bro");

})(jQuery); // End of use strict

// Disable Google Maps scrolling
// See http://stackoverflow.com/a/25904582/1607849
// Disable scroll zooming and bind back the click event
var onMapMouseleaveHandler = function(event) {
  var that = $(this);
  that.on('click', onMapClickHandler);
  that.off('mouseleave', onMapMouseleaveHandler);
  that.find('iframe').css("pointer-events", "none");
}
var onMapClickHandler = function(event) {
  var that = $(this);
  // Disable the click handler until the user leaves the map area
  that.off('click', onMapClickHandler);
  // Enable scrolling zoom
  that.find('iframe').css("pointer-events", "auto");
  // Handle the mouse leave event
  that.on('mouseleave', onMapMouseleaveHandler);
}
// Enable map zooming with mouse scroll when the user clicks the map
$('.map').on('click', onMapClickHandler);

$(document).ready( function () {
	var origin   = window.location.origin;
	var invoice  = $("#invoice").val();
	var token	 = $("#token").val();
	var url 	 =  "/check_order";

    $("#checkOrderButton").on("click", function(){
    	$.ajax({
	      url: url,
		    dataType: 'text',
		    type: 'post',
		    contentType: 'application/json',
		    data: JSON.stringify({invoice: invoice, token: token}),
	      success:function(data){
          data = JSON.parse(data);
	      	$("#statuses").html(data);
          console.log(data);
	      	if (data["meta"]["code"] != 200 ) {
	      		$("#dataOrder").html("");
	      		$("#dataOrder").append("<div class='divider div-transparent div-arrow-down'></div>");
	      		$("#dataOrder").append("<h2>"+data['meta']['message']+"</h2>");
	      	}else{
	      		$("#dataOrder").html("");
            $("#dataOrder").append("<div class='divider div-transparent div-arrow-down'></div>");
            $("#dataOrder").append("<h2>"+data['data']['order_status']+"</h2>");
	      	}

	        return true
	      },
	      error: function(data){
	      	return false
	      }
	    })
    });
});
