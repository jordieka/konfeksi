module ApplicationHelper

  @@statuses = [ "success", "info", "primary", "warning", "danger", "secondary", "light", "dark" ]

  def get_page_action(name)
    case name
    when "index"
      return "List"
    when "add"
      return "Create"
    when "edit"
      return "Change"
    when "delete"
      return "Delete"
    when "show"
      return "Detail"
    else
      return name.to_s.gsub("_", " ").split.map(&:capitalize)*' '
    end
  end

  def get_badge(data, status)
    
    if !@@statuses.include?(status)
      status = "info"
    end

    return "<div class='badge badge-"+status+"'>"+data+"</div>"
  end

  def get_badge_request(data)
    case data.to_s.downcase
    when "pending"
      status = "warning"
    when "approve"
      status = "success"
    when "rejected"
      status = "danger"
    when "partly_complete"
      status = "info"
    when "done"
      status = "success"
    else
      status = "light"
    end

    data = data.to_s.gsub("_", " ").split.map(&:capitalize)*' '

    return "<div class='badge badge-"+status+"'>"+data+"</div>"
  end

  def get_badge_payment(data)
    case data.to_s.downcase
    when "dp"
      status = "warning"
    when "paid_off"
      status = "success"
    end

    data = data.to_s.gsub("_", " ").split.map(&:upcase)*' '

    return "<div class='badge badge-"+status+"'>"+data+"</div>"
  end

  def get_badge_transaction(data)
    case data.to_s.downcase
    when "pending"
      status = "secondary"
    when "payment_pending"
      status = "warning"
    when "paid"
      status = "info"
    when "done"
      status = "success"
    else
      status = "light"
    end

    data = data.gsub("_", " ")

    return "<div class='badge badge-"+status+"'>"+data+"</div>"
  end

  def get_badge_order(data)
    case data.to_s.downcase
    when "pending"
      status = "dark"
    when "ready_production"
      status = "info"
    when "material_requested"
      status = "info"
      when "material_ready"
      status = "info"
    when "on_progress"
      status = "info"
    when "finish_production"
      status = "primary"
    when "sending"
      status = "light"
    when "success"
      status = "info"
    when "cancel"
      status = "danger"
    when "paid"
      status = "success"
    else
      status = "light"
    end

    data = data.gsub("_", " ")

    return "<div class='badge badge-"+status+"'>"+data+"</div>"
  end

  def get_badge_production(data)
    case data.to_s.downcase
    when "designing"
      status = "dark"
    when "patterning"
      status = "info"
    when "embroidering"
      status = "warning"
      when "printing"
      status = "warning"
    when "cutting"
      status = "danger"
    when "sewing"
      status = "light"
    when "finishing"
      status = "success"
    when "complete"
      status = "success"
    else
      status = "light"
    end

    data = data.gsub("_", " ")

    return "<div class='badge badge-"+status+"'>"+data+"</div>"
  end

  def get_badge_position(data)
    case data.to_s.downcase
    when "superadmin"
      status = "dark"
    when "sekretaris"
      status = "info"
    when "pengawas_produksi"
      status = "warning"
    when "pengawas_konfeksi"
      status = "secondary"
    when "penjahit"
      status = "danger"
    when "cutter"
      status = "light"
    when "finisher"
      status = "success"
    when "embordier_printer"
      status = "primary"
    when "owner"
      status = "success"
    else
      status = "light"
    end

    data = data.gsub("_", " ")

    return "<div class='badge badge-"+status+"'>"+data.upcase+"</div>"
  end

  def random_strings(length)
    o = [('a'..'z'), ('A'..'Z'), (0..9)].map(&:to_a).flatten
    string = (0...length).map { o[rand(o.length)] }.join
  end

  def last_login
    now = DateTime.now.to_i
    last_login = current_user.last_login.to_i
    remaining = now - last_login

    if remaining > 59
      
      remaining = remaining / 60

      if remaining  > 60

        remaining = remaining / 60

        if remaining > 24
          result = last_login.strftime("%d %B %Y at %l:%M %P")
        else
          result = remaining.to_s + " hour"
        end

      else 
        result = remaining.to_s + " minute"
      end

    else
      result = remaining.to_s + " second"
    end


    return result + " ago"
  end
end
