class AvatarStringIO < StringIO
  def original_filename
  	data = "img_" + Time.now.strftime("%Y%m%d%H%M%S").to_s
	return data + ".png"
  end
end