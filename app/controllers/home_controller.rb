class HomeController < ApplicationController
  skip_before_action :authenticate_user!
  layout "landing"

  def index

  end

  def check_order    
  	respond_to do |f|
      f.json do
        order = Order.find_by(invoice: params[:invoice], token: params[:token])
        if order.present?
          return render json: { data: {order: order, order_item: order.order_items, order_status: order.order_status.to_s.gsub("_", " ").split.map(&:capitalize)*' '}, meta: { code: 200, description: "Not Found", message: "Order ready!" } }
        else
          return render json: { data: {}, meta: { code: 404, description: "Not Found", message: "Order, tidak ditemukan!" } }
        end

      	return render json: { data: {}, meta: { code: 404, description: "Not Found", message: "Feature, is coming soon!" } }
      end
    end
  end

  def access_denied
    
  end

end
