class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!

  rescue_from CanCan::AccessDenied do |exception|
    # respond_to do |format|
    # format.json { head :forbidden }
    #   format.html { redirect_to access_denied_path, :alert => exception.message }
    # end

    render :file => "#{Rails.root}/public/403.html", :status => 403, :layout => false
  end

  def authenticate_role
  	if current_user.role.eql?("superadmin")
  		redirect_to rails_admin_path 
    end
  end

  def after_sign_in_path_for(resource)
    flash[:success] = "Login success!"
  	update_last_login = current_user.update(last_login: Time.now) 
    if current_user.role != "superadmin"
    	web_admin_dashboard_path
    else
    	rails_admin_path
    end
  end
end
