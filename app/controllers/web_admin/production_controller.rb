class WebAdmin::ProductionController < ApplicationController
	before_action :authenticate_role
	before_action :get_orders, only: [:index]
	before_action :get_order_items, only: [:create_progress, :show_logs, :show]
	before_action :get_order, only: [:start_production]
	before_action :get_order_item_size, only: [:create]
	before_action :get_log, only: [:show, :show_logs]
	before_action :check_complete, only: [:create_progress]
	layout "web_admin"

	def index
		@modal = "detail_order_items"
	end
	
	def create_progress
		@modal = "production_logs"
	end

	def create
		create = ProductionLog.create_production_log(@order_item_size, log_params)

		if create[0] == true
			flash[:success] = create[1]
		else
			flash[:error] = create[1]
		end
		
		redirect_to progress_web_admin_productions_path(@order_item_size.order_item.order.invoice)
	end

	def start_production
	  if @order.update!(order_status: :on_progress)
	  	OrderMailer.order_ready(@order)
		flash[:success] = "Order are ready to produce!"
      else
        flash[:error] = "Error occured!"
      end

      redirect_to list_web_admin_productions_path
	end

	def show
		@logs = @logs.order(created_at: :desc)
	end

	def get_sizes
		@order_item = OrderItem.find_by(id: params[:id])
		@get_items = []

		@order_item.order_item_sizes.each do |size|
			data = {
				id: size.id,
				size: size.product_size.size
			}

			@get_items << data
		end

        render json: {"items" => @get_items, "qnt" => @order_item.order_item_sizes.first.quantity}
	end

	def show_size
		@order_item = OrderItem.find_by(id: params[:id])
		@get_items = @order_item.order_item_sizes.find_by(size: params[:size])

        render json: @get_items
	end

	def show_logs
		data = []

		@logs.each do |log|
			a = {
				created_at: log.created_at,
				status: log.progress_status,
				quantity_finish: log.quantity_finish,
				product: "#{log.order_item_size.order_item.product.name} - #{log.order_item_size.product_size.size}"
			}

			data << a
		end

		render json: data
	end

	private

	def get_orders
		@orders = Order.active.prod_ready
	end

	def get_order_items
		@order = Order.find_by(invoice: params[:invoice])
		@order_items = @order.order_items
	end

	def get_order
		@order = Order.find_by(id: params[:id])
	end

	def get_order_item_size
		@order_item_size = OrderItemSize.find_by(id: params[:size])

		if !@order_item_size.present?
        	flash[:error] = "Order Item not found!"
			redirect_to list_web_admin_productions_path
		end
	end

	def get_log
		ids = []
		@order_items.each do |item|
			item.order_item_sizes.each do |size|
				ids << size.id
			end
		end

		@logs = ProductionLog.where(order_item_size_id: ids).order(created_at: :desc)
	end

	def check_complete
		order = Order.find_by(invoice: params[:invoice])
		status = "not complete"
		order.order_items.each do |item|
			last = item.production_logs.last
			if last.progress_status.eql?("complete")
				status = "complete"
			else
				break
			end
			Rails.logger.info "============= #{item.id} ============ #{last} ============== #{status}"
		end

		if status.eql?("complete")
			order.update(order_status: :finish_production)
			redirect_to list_web_admin_productions_path
		end
	end

	def log_params
		params.permit(:quantity_done, :size, :item, :image, :progress_status, :description => [])
	end
end
