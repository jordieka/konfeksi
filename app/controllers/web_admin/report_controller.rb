class WebAdmin::ReportController < ApplicationController
	layout "web_admin"

	def employee
		@list = Personnel.where("position != ?", 0)
	end

	def material_request
		@list = MaterialRequest.active
		if params[:filter].present?
			@filter = Date.parse("#{params[:filter]["year"]}-#{params[:filter]["month"]}-01") rescue nil
			@list = MaterialRequest.active.where(created_at: @filter.beginning_of_month..@filter.end_of_month) if !@filter.nil?
		end

		@list = MaterialRequest.active if params[:query].eql?("all")

		respond_to do |format|
			@data_type = "all"
			if @filter.present?
				@data_type =  @filter.strftime("%Y-%B").to_s.downcase + "-monthly" 
			end
			filename = "report-material-request-"+@data_type+"-"+ DateTime.now.strftime("%Y_%m_%d_%H_%M_%S") + ".xlsx"
			format.xlsx {
				response.headers[
				'Content-Disposition'
				] = "attachment; filename=#{filename}"
			}
			format.html { render :material_request }
		end
	end

	def material_transaction
		@list = MaterialTransaction.active
		if params[:filter].present?
			@filter = Date.parse("#{params[:filter]["year"]}-#{params[:filter]["month"]}-01") rescue nil
			@list = MaterialTransaction.active.where(created_at: @filter.beginning_of_month..@filter.end_of_month) if !@filter.nil?
		end

		@list = MaterialTransaction.active if params[:query].eql?("all")

		respond_to do |format|
			@data_type = "all"
			if @filter.present?
				@data_type =  @filter.strftime("%Y-%B").to_s.downcase + "-monthly" 
			end
			filename = "report-material-transaction-"+@data_type+"-"+ DateTime.now.strftime("%Y_%m_%d_%H_%M_%S") + ".xlsx"
			format.xlsx {
				response.headers[
				'Content-Disposition'
				] = "attachment; filename=#{filename}"
			}
			format.html { render :material_transaction }
		end
	end

	def order
		@list = Order.active
		if params[:filter].present?
			@filter = Date.parse("#{params[:filter]["year"]}-#{params[:filter]["month"]}-01") rescue nil
			@list = Order.active.where(created_at: @filter.beginning_of_month..@filter.end_of_month) if !@filter.nil?
		end

		@list = Order.active if params[:query].eql?("all")

		respond_to do |format|
			@data_type = "all"
			if @filter.present?
				@data_type =  @filter.strftime("%Y-%B").to_s.downcase + "-monthly" 
			end
			filename = "report-order-"+@data_type+"-"+ DateTime.now.strftime("%Y_%m_%d_%H_%M_%S") + ".xlsx"
			format.xlsx {
				response.headers[
				'Content-Disposition'
				] = "attachment; filename=#{filename}"
			}
			format.html { render :order }
		end
	end

	def production
		
	end

	def user_report
		@reports = Report.newest
	end

	def view
		@report = Report.find_by(id: params[:id])
		if @report.present?
			@report.update(is_read: true, read_date: Time.now) if !@report.is_read
		else
			redirect_to user_report_web_admin_reports_path 
		end
	end

	def create
		
	end

	def post_create
		@report = Report.create_report(report_params, current_user)

		if @report
			flash[:success] = "Report sucessfully created."
		else
		  flash[:error] = "Report can't be create."
		end
  
		redirect_to create_web_admin_reports_path
	end

	private

	def report_params
		params.permit(:subject, {:report => []})
	end

end
