module WebAdmin
	class CategoryController < ApplicationController
		before_action :authenticate_role
		before_action :get_category, only: [:update, :delete]
	    load_and_authorize_resource
	    layout 'web_admin'

	    def index
	    	@list = Category.active
      		@modal = "update_category"
	    end

	    def create
	    	if Category.create!(category_params)
	    		flash[:success] = "Category berhasil ditambahkan"
			else
				flash[:error] = "Category gagal ditambahkan"
			end

	    	redirect_to list_web_admin_categories_path
	    end

	    def update
	    	if @category.update!(category_params)
	    		flash[:success] = "Category berhasil diubah"
			else
				flash[:error] = "Category gagal diubah"
			end

	    	redirect_to list_web_admin_categories_path
	    end

	    def delete
	    	if @category.update!(row_status: 0)
	    		flash[:success] = "Category berhasil dihapus"
			else
				flash[:error] = "Category gagal dihapus"
			end

	    	redirect_to list_web_admin_categories_path
	    end

	    private

	    def category_params
	    	params.permit(:name)
	    end

	    def get_category
	    	@category = Category.find_by(id: params[:category]) || Category.find_by(id: params[:id])
	    end
	end
end
