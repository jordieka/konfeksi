module WebAdmin
  class SupplierController < ApplicationController
    before_action :authenticate_role
    before_action :get_supplier, only: [:edit, :update, :delete]
    load_and_authorize_resource
    layout 'web_admin'

    def index
      @list = Supplier.active
    end

    def add

    end

    def create
      if Supplier.create!(supplier_params)
        flash[:success] = "Suppliers berhasil ditambahkan"
      else
        flash[:error] = "Suppliers gagal ditambahkan"
      end

      redirect_to web_admin_suppliers_path
    end

    def edit

    end

    def update
      if @supplier.update!(supplier_params)
        flash[:success] = "Suppliers berhasil ditambahkan"
      else
        flash[:error] = "Suppliers gagal ditambahkan"
      end

      redirect_to web_admin_suppliers_path
    end

    def delete
      if @supplier.update!(row_status: 0)
        flash[:success] = "Suppliers berhasil ditambahkan"
      else
        flash[:error] = "Suppliers gagal ditambahkan"
      end

      redirect_to web_admin_suppliers_path
    end

    private

    def supplier_params
      params.permit(:name, :phone_number, :address)
    end

    def get_supplier
      @supplier = Supplier.find_by(id: params[:id])
    end
  end
end
