include ActionView::Helpers::NumberHelper

module WebAdmin
  class OrderController < ApplicationController
    before_action :authenticate_role
    before_action :get_customer, :get_product, only: [:add, :add_negotiation]
    before_action :get_order, only: [:manage, :order_items, :delete, :export_pdf]
    before_action :get_order_item, only: [:edit, :detail]
    layout 'web_admin'
    
    def index
        @list = Order.active.order(created_at: :desc)
        @modal = "order_update_status"
    end

    def add
        
    end

    def add_negotiation
        
    end

    def create_negotiation
        o = [('a'..'z'), ('A'..'Z'), (0..9)].map(&:to_a).flatten
        string = (0...10).map { o[rand(o.length)] }.join

        if Order.create_negotiation(params, current_user.id, string)
            flash[:success] = "Order succesfully created!"
        else
            flash[:error] = "Order failed to create!"
        end

        redirect_to list_web_admin_orders_path
    end

    def create
        o = [('a'..'z'), ('A'..'Z'), (0..9)].map(&:to_a).flatten
        string = (0...10).map { o[rand(o.length)] }.join

        if Order.create_orders(order_params, current_user.id, string)
            flash[:success] = "Order succesfully created!"
        else
            flash[:error] = "Order failed to create!"
        end

        redirect_to list_web_admin_orders_path
    end

    def manage
        @items = @order.order_items
    end

    def export_pdf

        respond_to do |format|
            format.pdf do
                render pdf: "Order #{@order.invoice}",
                page_size: 'A4',
                layout: "pdf.html",
                orientation: "Landscape",
                lowquality: true,
                zoom: 1,
                dpi: 75
            end
        end
    end

    def edit

    end

    def detail
        
    end

    def update
        update = OrderItem.find_by(id: params[:id])
        
        if update.update_order(order_item_params)
            flash[:success] = "Order Item succesfully updated!"
        else
            flash[:error] = "Order Item failed to update!"
        end

        redirect_to manage_web_admin_orders_path(update.order.invoice)
    end

    def update_finish_production
        order  = Order.find_by(invoice: params[:invoice])
        update = order.update_finish_production(params)

        if update[0] == true
            flash[:success] = update[1]
        else
            flash[:error] = update[1]
        end

        redirect_to list_web_admin_orders_path
    end

    def delete
        if @prder_items.update_all(row_status: 0)
            if @order.update(row_status: 0)
                flash[:success] = "Order succesfully delete!"
            else
                flash[:error] = "Order cannot be delete!"
            end
        end
        
    end

    def update_status
        item = OrderItem.find_by(id: params[:id])

        if item.is_bordir.nil? and item.is_sablon.nil?
            flash[:error] = "You haven't set the item yet!"
        else

            status = true

            if item.is_sablon
                
                if item.sablon_position.nil? || item.sablon_image.nil? || item.sablon_price.nil?
                    flash[:error] = "Print data is still not set!" 
                    status = false
                end

            elsif item.is_bordir
                
                if item.bordir_position.nil? || item.bordir_image.nil? || item.sablon_price.nil?
                    flash[:error] = "Embriodery data is still not set!" 
                    status = false
                end
            end

            if status == true
                item.update(production_status: :on_progress)
                flash[:success] = "Order Item are ready for productions!"

                order = OrderItem.where(order_id: item.order_id, production_status: :pending)
                if !order.present?
                    item.order.update(order_status: :ready_production)
                end
            end
            
        end

        redirect_to manage_web_admin_orders_path(item.order.invoice)
    end

    def order_items
        @get_items = []

        @order.order_items.each do |item|
            data = {
                id: item.id,
                product_name: item.product.name,
                sub_total: "Rp. #{number_with_delimiter(item.sub_total, delimiter: ".")}",
                quantity: item.quantity,
                production_status: item.production_status.to_s.gsub("_", " ").split.map(&:capitalize)*' '
            }

            @get_items << data
        end

        render json: @get_items
    end

    private

    def order_params
        params.permit(:dp, :orders, :customer)
    end

    def order_item_params
        params.permit(:is_bordir, :is_sablon, :bordir_image, :sablon_image, :pattern_image, {:description => []}, :bordir_position, :sablon_position, :bordir_price, :sablon_price)
    end

    def get_customer
        @customers = Customer.active
    end

    def get_product
        @products  = Product.active.has_product_materials.order(id: :asc)
        @product_sizes = ProductSize.active.where(product_id: @products.first.id).order(size: :asc)
    end

    def get_order
        @order = Order.find_by(invoice: params[:invoice])
    end

    def get_order_item
        @item = OrderItem.find_by(id: params[:id])
    end
  end
end
