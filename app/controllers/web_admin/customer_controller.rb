module WebAdmin
  class CustomerController < ApplicationController
    before_action :authenticate_role
    before_action :get_customer, only: [:edit, :update, :delete, :show]
    load_and_authorize_resource
    layout 'web_admin'

    def index
      @list = Customer.active
    end

    def add

    end

    def create
      if Customer.create!(customer_params)
        flash[:success] = "Customer berhasil ditambahkan"
      else
        flash[:error] = "Customer gagal ditambahkan"
      end

      redirect_to web_admin_customers_path
    end

    def show
      
    end

    def edit

    end

    def update
      if @customer.update!(customer_params)
        flash[:success] = "Customer berhasil diubah"
      else
        flash[:error] = "Customer gagal diubah"
      end

      redirect_to web_admin_customers_path
    end

    def delete
      if @customer.update!(row_status: 0)
        flash[:success] = "Customer berhasil dihapus"
      else
        flash[:error] = "Customer gagal ditambahkan"
      end

      redirect_to web_admin_customers_path
    end

    private

    def customer_params
      params.permit(:full_name, :email, :company_name, :company_phone, :id_card, :phone_number, :address)
    end

    def get_customer
      @customer = Customer.find_by(id: params[:id])
    end

  end
end
