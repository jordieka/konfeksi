module WebAdmin
  class MaterialController < ApplicationController
    before_action :authenticate_role
    before_action :get_material, only: [:edit, :update, :delete]
    before_action :get_request, only: [ :request_material_detail, :request_buy_update ]
    before_action :sanitize_params
    layout 'web_admin'

    def index
      @list = Material.active
    end

    def add

    end

    def create
      if Category.find_by(id: params[:category])
        flash[:error] = "Category did not exist!" 
        redirect_to web_admin_materials_path
      end

      if Material.create!(material_params)
        flash[:success] = "Materials berhasil ditambahkan"
      else
        flash[:error] = "Materials gagal ditambahkan"
      end

      redirect_to web_admin_materials_path
    end

    def edit

    end

    def update
      if @material.update!(material_params)
        flash[:success] = "Materials berhasil diubah"
      else
        flash[:error] = "Materials gagal diubah"
      end

      redirect_to web_admin_materials_path
    end

    def delete
      if @material.update!(row_status: 0)
        flash[:success] = "Materials berhasil dihapus"
      else
        flash[:error] = "Materials gagal dihapus"
      end

      redirect_to web_admin_materials_path

    end

    def request_material_add
      if params[:invoice].present?
          @orders = Order.where(invoice: params[:invoice])
          @materials = @orders.first.get_material if @orders.present?
      end
    end

    def request_buy_create
      if MaterialRequest.create_request(request_params, current_user)
        flash[:success] = "Request bahan berhasil ditambahkan"
      else
        flash[:error] = "Request bahan gagal ditambahkan, terjadi kesalahan atau masih terdapat request pending."
      end

      redirect_to list_request_web_admin_materials_path
    end

    def request_material_detail

    end

    def request_buy_update
      update = @request.update_request(params, current_user)
      if update[0] == true
        flash[:success] = "Request berhasil diubah"
      else
        flash[:error] = update[1]
      end

      redirect_to list_request_web_admin_materials_path
    end

    def request_material_list
      @list = MaterialRequest.active.newest
    end

    private

    def sanitize_params
      params[:category] = params[:category].to_i
    end

    def material_params
      params.permit(:name, :material_use, :common_price, :unit, :category_id)
    end

    def request_params
      params.permit(:quantity, :status, :invoice, :items)
    end

    def get_material
      @material = Material.find_by(id: params[:id])
    end

    def get_request
      @request = MaterialRequest.active.find_by(id: params[:id])
    end
  end
end
