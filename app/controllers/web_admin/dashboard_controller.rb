module WebAdmin
  class DashboardController < ApplicationController
    before_action :authenticate_role
    before_action :get_user, only: [:update_password, :update_profile]
    layout 'web_admin'

    def index 
      @orders = Order.active
      @material_transactions = MaterialTransaction.active
      @productions = ProductionLog.all
    end

    def profile
      @user = current_user
    end

    def absent
      @absents = current_user.absents
      if params[:filter].present?
        @filter = Date.parse("#{params[:filter]["year"]}-#{params[:filter]["month"]}-01") rescue nil
        @list = @absents.where(created_at: @filter.beginning_of_month..@filter.end_of_month) if !@filter.nil?
      end

      @absents = current_user.absents if params[:query].eql?("all")

      respond_to do |format|
        filename = "report-material-transaction-" + DateTime.now.strftime("%Y_%m_%d_%H_%M_%S") + ".xlsx"
        format.xlsx {
          response.headers[
          'Content-Disposition'
          ] = "attachment; filename=#{filename}"
        }
        format.html { render :absent }
      end
    end

    def update_profile
      if @user.update_personnel_data(personnel_params)
        flash[:success] = "Profil berhasil diubah"
      else
        flash[:error] = "Profil gagal diubah"
      end

      redirect_to web_admin_profile_path
    end

    def update_password
      password = @user.update_password(password_params)

      if password[0] == true
        flash[:success] = "Password berhasil diubah"
      else
        if password[1].eql?("old")
          flash[:error] = "Password lama tidak sesuai"
        else
          flash[:error] = "Konfirmasi password tidak sesuai"
        end
      end

      redirect_to web_admin_profile_path
    end

    private

    def personnel_params
      params.permit(:email, :full_name, :birth_date, :gender, :phone_number)
    end

    def password_params
      params.permit(:old_password, :new_password, :confirm_password)
    end

    def get_user
      @user = current_user
    end

  end
end
