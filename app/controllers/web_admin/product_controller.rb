module WebAdmin
	class ProductController < ApplicationController
	before_action :authenticate_role
	before_action :get_category, only: [:add, :edit]
	before_action :get_product, only: [:edit, :update, :material, :create_material]
	before_action :get_material, only: [:material]
	layout 'web_admin'
	
	def index
		@list = Product.active.order(created_at: :asc)
	end

	def add
		
	end

	def edit
		
	end

	def delete
		
	end

	def show
		@product = Product.find_by(id: params[:id])

		redirect_to list_web_admin_products_path unless @product.present?
	end

	def create
		produk = Product.create_product(product_params, current_user.id)
		if produk[0] == true
			flash[:success] = produk[1]
			redirect_to list_web_admin_products_path
		else
			flash[:error] = produk[1]
			redirect_to add_web_admin_products_path
		end
	end

	def update
		produk = @product.edit_product(product_params, current_user.id)
		
		if produk[0] == true
			flash[:success] = produk[1]
			redirect_to list_web_admin_products_path
		else
			flash[:error] = produk[1]
			redirect_to edit_web_admin_products_path(@product.id)
		end
	end

	def material

	end

	def create_material
		if params[:items].empty?
			flash[:error] = "No data change!"
			redirect_to material_web_admin_products_path(@product.id)
		else
			if @product.add_material(material_params)
				flash[:success] = "Product material updated!"
			else
				flash[:error] = "Datas can't be changed!"
			end

			redirect_to detail_web_admin_products_path(@product.id)
		end
	end

	def get_product_by_id
		@get_data = Product.active.find_by(id: params[:id])

		render json: @get_data
	end

	def get_size
		@get_size = ProductSize.active.where(product_id: params[:id])

		render json: @get_size
	end

	private

	def get_category
		@category = Category.active
	end

	def get_product
		@product = Product.active.find_by(id: params[:id].to_i)
	end

	def get_material
		@material = Material.active.where(category_id: @product.category_id)
	end

	def product_params
		params.permit(:name, :base_price, :margin_price, :gender, :category, {:margins => []}, :sizes, :pattern_image)
	end

	def material_params
		params.permit(:items, :id)
	end
end

end