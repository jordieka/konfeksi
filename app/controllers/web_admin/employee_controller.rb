module WebAdmin
  class EmployeeController < ApplicationController
    before_action :authenticate_role
    before_action :get_personnel, only: [:edit, :update, :delete]
    layout 'web_admin'

    def index
      @list = Personnel.active
      @list = @list.for_secretary.where("id != ?", current_user.personnel.id) if current_user.role.eql?("sekretaris")
    end

    def add

    end

    def create
      if Personnel.create_employee(personnel_params)
        flash[:success] = "Personel successfully added."
      else
        flash[:error] = "Personel failed to create."
      end

      redirect_to web_admin_employees_path
    end

    def edit

    end

    def update
      if @personnel.update!(personnel_params)
        flash[:success] = "Persnonel successfully changed."
      else
        flash[:error] = "Personnel failed to change."
      end

      redirect_to web_admin_employees_path
    end

    def delete
      if @personnel.update!(row_status: 0)
        flash[:success] = "Personnel successfully deleted."
      else
        flash[:error] = "Personnel can't be delete."
      end

      redirect_to web_admin_employees_path
    end

    def salary_recap
      @list = Personnel.active.exclude_owner
    end

    private

    def personnel_params
      params.permit(:full_name, :email, :position, :birth_date, :phone_number, :gender, :salary, :salary_type)
    end

    def get_personnel
      @personnel = Personnel.find_by(id: params[:id])
    end

  end
end
