module WebAdmin
  class InventoryController < ApplicationController
    before_action :authenticate_role
    before_action :get_request, only: [ :request_buy_detail, :request_buy_update  ]
    before_action :get_transaction, only: [ :transaction_detail]
    layout 'web_admin'

    def request_buy_add

    end

    def request_buy_create
      if MaterialProcurement.create_request(request_params, current_user)
        flash[:success] = "Request pembelian berhasil ditambahkan"
      else
        flash[:error] = "Request pembelian gagal ditambahkan, terjadi kesalahan atau masih terdapat request pending."
      end

      redirect_to list_request_web_admin_inventories_path
    end

    def request_buy_detail

    end

    def request_buy_update
      if @request.update_request(params, current_user)
        flash[:success] = "Request berhasil diubah"
      else
        flash[:error] = "Request gagal diubah"
      end

      redirect_to list_request_web_admin_inventories_path
    end

    def request_buy_list
      @list = MaterialProcurement.active.newest
    end

    def transaction_add
      @request = MaterialProcurement.active.by_status(:approve, "first")
    end

    def transaction_create
      transaction = MaterialTransaction.create_transaction(transaction_params, current_user) unless MaterialTransaction.active.where(order_status: [:pending, :payment_pending]).present?

      if transaction[0] == true
        flash[:success] = "Data transaksi berhasil ditambahkan"
      else
        flash[:error] = "Data transaksi gagal dimasukkan"
        flash[:message] = transaction[1] 
      end

      redirect_to list_transaction_web_admin_inventories_path
    end

    def transaction_list
      @list = MaterialTransaction.active.newest
      @modal = "transaction_update_status"
    end

    def transaction_detail

    end

    def transaction_update_status
      transaction = MaterialTransaction.update_status(update_status_params, current_user)

      if transaction[0] == true
        flash[:success] = "Status berhasil diubah"
      else
        flash[:error] = "Status gagal diubah"
        flash[:message] = transaction[1] 
      end

      redirect_to list_transaction_web_admin_inventories_path
    end

    private

    def request_params
      params.permit(:items, :quantity, :status)
    end

    def transaction_params
      params.permit({:quantities => []}, {:prices => []}, :bill_image, {:suppliers => []})
    end

    def update_status_params
      params.permit(:invoice, :payment_image, :inv)
    end

    def get_request
      @request = MaterialProcurement.active.find_by(id: params[:id])
    end

    def get_transaction
      @transaction = MaterialTransaction.active.find_by(id: params[:id])
    end

  end
end
