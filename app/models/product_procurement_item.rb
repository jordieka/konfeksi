# == Schema Information
#
# Table name: product_procurement_items
#
#  id                      :bigint           not null, primary key
#  quantity                :integer
#  row_status              :integer          default(1)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  materials_id            :bigint
#  product_procurements_id :bigint
#
# Indexes
#
#  index_product_procurement_items_on_materials_id             (materials_id)
#  index_product_procurement_items_on_product_procurements_id  (product_procurements_id)
#

class ProductProcurementItem < ApplicationRecord
end
