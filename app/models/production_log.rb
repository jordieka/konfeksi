# == Schema Information
#
# Table name: production_logs
#
#  id                 :bigint           not null, primary key
#  description        :text
#  image              :text
#  progress_status    :integer
#  quantity_finish    :integer
#  quantity_left      :integer
#  row_status         :integer          default(1)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  order_item_size_id :bigint
#
# Indexes
#
#  index_production_logs_on_order_item_size_id  (order_item_size_id)
#
# Foreign Keys
#
#  fk_rails_...  (order_item_size_id => order_item_sizes.id) ON DELETE => cascade
#

class ProductionLog < ApplicationRecord
	attr_accessor :image
  	mount_uploader :image, ImageUploader
	
	belongs_to :order_item_size

	enum progress_status: [ :designing, :patterning, :embroidering, :printing, :cutting, :sewing, :finishing, :complete ]

	def self.create_production_log(order_item_size, params)
			
		qnt = params[:quantity_done].to_i

		if qnt > order_item_size.quantity
			return false, "Quantity can't be more than ordered items!"
		elsif !progress_statuses.include?(params[:progress_status])
			return false, "Status cannot be found"
		end
		
		if params[:progress_status].eql?("designing") || params[:progress_status].eql?("patterning")
			qnt = 0
		end
		
		logger = ProductionLog.where(order_item_size_id: order_item_size.id).order(id: :asc)
		if logger.present?
			last = logger.last

			val = logger.pluck(:progress_status)
			cur_stat = val.map { |a| ProductionLog.progress_statuses[a] }
			set_stat = ProductionLog.progress_statuses[params[:progress_status]]

			if cur_stat.max == 7
				return false, "This production item is complete!"
			elsif (set_stat == 0 || set_stat == 1) and cur_stat.max == set_stat
				return false, "The status of this order item hal already been done."
			elsif set_stat == 2
				return false, "This order item did not include embroidering" unless order_item_size.order_item.is_bordir
			elsif set_stat == 3
				return false, "This order item did not include printing" unless order_item_size.order_item.is_sablon
			elsif set_stat != cur_stat.max and last.quantity_left > 0
				Rails.logger.info "#{last}"
				return false, "This production are not finish yet! Last status : #{last.progress_status.capitalize}"
			elsif set_stat < cur_stat.max
				return false, "Can't go back to previous status! Last status : #{last.progress_status.capitalize}"
			end

			if qnt <= last.quantity_left and cur_stat.max == set_stat
				Rails.logger.info "========== 1"
				left = (last.quantity_left - qnt) 
			elsif last.quantity_left <= 0 and cur_stat.max == set_stat
				return false, "This item has been complete for this status!"
			elsif cur_stat.max != set_stat
				Rails.logger.info "========== 2"
				left = (order_item_size.quantity - qnt)
			else
				return false, "Your input quantity are bigger than remaining quantity!"
			end

			if ((set_stat - 1) == cur_stat.max or cur_stat.max == set_stat) or set_stat == 3

				case set_stat
				when 2
				 	if cur_stat.max == set_stat or set_stat == 3 or set_stat == 4
						status = params[:progress_status]
				 	else
				 		Rails.logger.info "kena 1"
				 		return false, "Status must match the order." 
				 	end
				when 3
					if cur_stat.max == 1 or cur_stat.max == set_stat or set_stat == 4
						status = params[:progress_status]
				 	else
				 		Rails.logger.info "kena 2"
				 		return false, "Status must match the order." 
				 	end
				else
					status = params[:progress_status]
				end

			else
				if cur_stat.max == 1 and set_stat == 4
					if order_item_size.order_item.is_bordir or order_item_size.order_item.is_sablon
						return false, "Status must match the order."
					else
						status = params[:progress_status]
					end
				else
					Rails.logger.info "kena 4"
					return false, "Status must match the order."
				end
			end
			

		else
			left = (order_item_size.quantity - qnt)
			status = :designing
		end

		if params[:image].present?
			image = params[:image]
		  	encoded_image = Base64.encode64 image.read
		  	io = AvatarStringIO.new(Base64.decode64(encoded_image))

		  	image = MiniMagick::Image.open(image.path)

		  	valid_image = image.valid? rescue false
		else
		 	valid_image = true
		end
		

	  	if valid_image
	  		log = ProductionLog.create!(
				description: params[:description].first,
				progress_status: status,
				quantity_finish: qnt,
				quantity_left: left,
				row_status: 1,
				order_item_size_id: order_item_size.id,
				image: io
			)

			if log.progress_status.eql?("finishing") and log.quantity_left == 0 
				ProductionLog.create!(
					description: "",
					progress_status: :complete,
					quantity_finish: order_item_size.quantity,
					quantity_left: nil,
					row_status: 1,
					order_item_size_id: order_item_size.id
				)
			end
			
		else
			return false, "You insert an invalid image"
	  	end
		
		if log
			return true, "Production data has been saved!"
		else
			return false, "There are errors on the system!"
		end
		

	end
end
