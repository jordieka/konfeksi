# == Schema Information
#
# Table name: material_request_items
#
#  id                   :bigint           not null, primary key
#  is_complete          :boolean          default(FALSE)
#  qnt_left             :float
#  quantity             :float
#  row_status           :integer          default(1)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  material_id          :bigint
#  material_requests_id :bigint
#
# Indexes
#
#  index_material_request_items_on_material_id           (material_id)
#  index_material_request_items_on_material_requests_id  (material_requests_id)
#
# Foreign Keys
#
#  fk_rails_...  (material_id => materials.id) ON DELETE => cascade
#  fk_rails_...  (material_requests_id => material_requests.id) ON DELETE => cascade
#

class MaterialRequestItem < ApplicationRecord

  scope :active, -> { where(row_status: 1) }

  belongs_to :material_request, optional: true
  belongs_to :material, optional: true

  attr_reader :common_price

  after_validation :set_qnt_left, on: [ :create ]

  def common_price
  	self.material.common_price
  end

  def set_qnt_left
  	self.qnt_left = self.quantity
  end
  
end
