# == Schema Information
#
# Table name: product_sizes
#
#  id           :bigint           not null, primary key
#  margin_price :float
#  row_status   :integer          default(1)
#  size         :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  product_id   :bigint
#
# Indexes
#
#  index_product_sizes_on_product_id  (product_id)
#
# Foreign Keys
#
#  fk_rails_...  (product_id => products.id)
#
class ProductSize < ApplicationRecord

  enum size: [:xs, :s, :m, :l, :xl, :xxl]
  
  belongs_to :product
  has_many :order_item_sizes

  scope :active, -> { where(row_status: 1) }


end
