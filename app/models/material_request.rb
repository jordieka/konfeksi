# == Schema Information
#
# Table name: material_requests
#
#  id           :bigint           not null, primary key
#  is_change    :boolean
#  request_date :datetime
#  review_by    :integer
#  row_status   :integer          default(1)
#  status       :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  order_id     :bigint
#  user_id      :bigint
#
# Indexes
#
#  index_material_requests_on_order_id  (order_id)
#  index_material_requests_on_user_id   (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (order_id => orders.id) ON DELETE => cascade
#  fk_rails_...  (user_id => users.id) ON DELETE => cascade
#

class MaterialRequest < ApplicationRecord

  has_many :material_request_items, foreign_key: "material_requests_id"
  belongs_to :user, optional: true
  belongs_to :order, optional: true

  scope :active, -> { where(row_status: 1) }

  enum status: [ :pending, :approve, :rejected, :partly_complete ]

  attr_reader :reviewer

  def self.newest
    self.order(created_at: :desc)
  end

  def self.by_status(status, condition)
    if condition.eql?("first")
      return self.where(status: status).first
    else
      return self.where(status: status).all
    end
  end

  def reviewer
    return User.find_by(id: self.review_by)
  end

  def self.create_request(params, current_user)
    get_order = Order.find_by(invoice: params[:invoice])

    if !get_order.present?
      return false
    end

    get_last = MaterialRequest.where(order_id: get_order.id)

    if get_last.where(status: :pending).present?
      return false
    else

      materials = {}
      get_order.get_material.each { |key, value| materials[key.to_s] = value[:qnt] }
      items = JSON.parse Base64.decode64(params[:items])
        
      Rails.logger.info "#{materials == items and !get_last.present?}"  

      if materials != items and !get_last.present?
        return false
      end
        
      request = MaterialRequest.create!(
        request_date: Time.now,
        review_by: User.find_by(role: "owner").email,
        status: :pending,
        is_change: false,
        order_id: get_order.id,
        user_id: current_user.id
      )

      if request
        items.each do |item|
          request.material_request_items.create!(
            material_id: item.first,
            quantity: item.second.to_f
          )
        end

        get_order.update(order_status: :material_requested) if get_order.order_status.eql?("ready_production")

        MaterialMailer.request_material(request).deliver_now

        return true
      else
        return false
      end
    end
  end

  def update_request(params, current_user)

    case params[:status].to_s
    when "tolak"
      status = :rejected
      self.update!(status: :rejected)
      return true, "Request dibatalkan!"
    when "setuju"
      status = :approve
    else
      return false, "Terjadi kesalahan, periksa data masukkan anda!"
    end

    is_change = false
    self.material_request_items.each_with_index do |item, i|
      if params[:quantity][i].to_f > item.material.stock
        return false, "No enough stock"
      elsif params[:quantity][i].to_f > item.qnt_left
        return false, "Quantity can't be bigger then requested"
      end

      is_change = true if params[:quantity][i].to_f != item.quantity.to_f
    end

    request = self.update!(review_by: current_user.id,is_change: is_change)

    if request
      complete = true
      self.material_request_items.each_with_index do |item, i|
        left = item.qnt_left.to_f - params[:quantity][i].to_f
        left_stock = item.material.stock.to_f - params[:quantity][i].to_f
        
        item.qnt_left = left  
        material = item.material
        material.stock = left_stock

        item.save!
        material.save!
        complete = false if left > 0
      end

      if complete
        self.update!(status: :approve)
      else
        self.update!(status: :partly_complete)
      end

      
      self.order.update(order_status: :material_ready) if self.order.order_status.eql?("material_requested")
      
      MaterialMailer.update_request_material(self).deliver_later!
      return true, "Request succesfully change"
    end
  end
end
