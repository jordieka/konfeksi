# == Schema Information
#
# Table name: personnels
#
#  id                :bigint           not null, primary key
#  birth_date        :datetime
#  email             :string
#  full_name         :string
#  gender            :integer
#  phone_number      :string
#  position          :integer
#  remaining_day_off :integer          default(12)
#  row_status        :integer          default(1)
#  salary            :float
#  salary_type       :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  user_id           :bigint
#
# Indexes
#
#  index_personnels_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id) ON DELETE => cascade
#

class Personnel < ApplicationRecord

  belongs_to :user, optional: true
  has_many   :absents

  enum position: [ :owner, :superadmin, :sekretaris, :pengawas_produksi, :pengawas_konfeksi, :penjahit, :cutter, :finisher, :embordier_printer]
  enum gender: [ :male, :female ]
  enum salary_type: [:monthly, :weekly, :daily]
  scope :active, -> { where(row_status: 1) }
  scope :for_secretary, -> { where("position != 0 and position != 1") }
  scope :exclude_owner, -> { where("position != 0") }

  def self.create_employee(params)

    if Personnel.positions.keys[0..4].include?(params[:position])
      check = Personnel.where(position: params[:position], row_status: 1)

      return false if check.present?
    end

    if User.roles.include?(params[:position])

      temp_password = random_strings(10)
      user = User.create!(
        email: params[:email],
        role: params[:position],
        password: temp_password,
        password_confirmation: temp_password,
        temp_password: temp_password
      )

      if user
        personnel = Personnel.create(
          user_id: user.id,
          full_name: params[:full_name],
          birth_date: params[:birth_date],
          email: params[:email],
          gender: params[:gender],
          phone_number: params[:phone_number],
          position: params[:position],
          salary: params[:salary],
          salary_type: params[:salary_type],
          user_id: user.id
        )
      else
        return false
      end
    else
      personnel = Personnel.create!(
        full_name: params[:full_name],
        birth_date: params[:birth_date],
        gender: params[:gender],
        email: params[:email],
        phone_number: params[:phone_number],
        position: params[:position],
        salary: params[:salary],
        salary_type: params[:salary_type]
      )
    end

    if personnel

      # UserMailer.new_user(personnel).deliver_now! if personnel.user.present?

      return true
    else
      return false
    end
  end

  private

  def self.random_strings(length)
    o = [('a'..'z'), ('A'..'Z'), (0..9)].map(&:to_a).flatten
    string = (0...length).map { o[rand(o.length)] }.join
  end

end
