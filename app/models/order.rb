# == Schema Information
#
# Table name: orders
#
#  id                     :bigint           not null, primary key
#  created_by             :integer
#  down_payment           :float
#  invoice                :string           not null
#  kurir                  :integer
#  order_status           :integer
#  payment_image          :text
#  payment_status         :integer          default("dp")
#  row_status             :integer          default(1)
#  shipping_invoice       :string
#  shipping_invoice_image :text
#  token                  :string
#  total                  :float
#  total_qnt              :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  customer_id            :bigint
#
# Indexes
#
#  index_orders_on_customer_id  (customer_id)
#
# Foreign Keys
#
#  fk_rails_...  (created_by => users.id) ON DELETE => cascade
#  fk_rails_...  (customer_id => customers.id) ON DELETE => cascade
#

class Order < ApplicationRecord

  enum order_status: [ :pending, :ready_production, :material_requested, :material_ready, :on_progress, :finish_production, :paid, :sending, :success, :cancel]
  enum kurir: [:internal, :jne, :jnt, :tiki, :pos]
  enum payment_status: [ :dp, :paid_off ]

  scope :active, -> { where(row_status: 1) }
  scope :prod_ready, -> { where("order_status != ? and order_status < ?", 0, 5) }
  scope :on_going, -> { where(order_status: [:on_progress, :finish_production, :sending]) }

  belongs_to :customer, optional: true
  belongs_to :user, optional: true
  has_many   :order_items
  has_many	 :material_requests

  def self.create_negotiation(params, create_by, inv)

  	order = Order.new

  	invoice = "INV" + Time.now.strftime("%Y%m%d%H%M%S").to_s
  	order.invoice = invoice
  	order.customer_id = params[:customer].to_i

  	if params[:pembayaran].eql?("lunas")
  		order.payment_status = :paid_off
  		order.down_payment = 0
  	else
  		if params[:total].to_f < params[:dp].to_f
	  		return false, "Down payment can't be higher than total"
	  	end
  		order.payment_status = :dp
  		order.down_payment = params[:dp].to_f
  	end

  	order.token = inv
  	order.order_status = 0
  	order.row_status = 1
  	
  	order.save!

  	if order
  		orders = JSON.parse Base64.decode64(params[:orders])
  		total = 0
	  	total_qnt = 0
	  	product = {}
		orders.each do |hash|
		  total_qnt += hash[1]["qnt"].to_i
		  total += (hash[1]["price"].to_i * hash[1]["qnt"].to_i)

		  item = ProductSize.find_by(id: hash[0].to_i)
		  if !order.order_items.find_by(product_id: item.product_id).present?

		  	sub_total = ((hash[1]["price"].to_f + hash[1]["bordir_price"].to_f  + hash[1]["sablon_price"].to_f) * hash[1]["qnt"].to_i)
		  	bordir = hash[1]["bordir_price"].blank? ? 0 : hash[1]["bordir_price"].to_f
		  	sablon = hash[1]["sablon_price"].blank? ? 0 : hash[1]["sablon_price"].to_f

		  	order_item = order.order_items.create!(
				product_id: item.product_id,
				row_status: 1,
				sub_total: sub_total,
				is_bordir: bordir > 0 ? true : false,
				is_sablon: sablon > 0 ? true : false,
				bordir_price: bordir > 0 ? bordir : nil,
				sablon_price: sablon > 0 ? sablon : nil,
				production_status: :pending
			 )
		  	product[order_item.id] = {qnt: hash[1]["qnt"].to_i, price: hash[1]["price"].to_f, sablon_price: hash[1]["sablon_price"].to_f, bordir_price: hash[1]["bordir_price"].to_f}
		  else
		  	order_item = order.order_items.find_by(product_id: item.product_id)
		  	product[order_item.id][:qnt] += hash[1]["qnt"].to_i
		  end

		  order_item.order_item_sizes.create(
		  	price_per_item: hash[1]["price"].to_i,
		  	quantity: hash[1]["qnt"].to_i,
		  	product_size_id: item.id
		  )
		end

		product.each do |item|
			i = OrderItem.find_by(id: item.first)
			i.quantity = item[1][:qnt]
			i.sub_total = (item[1][:price] + item[1][:bordir_price] + item[1][:sablon_price]) * item[1][:qnt]
			i.save
		end

		order.total = total
		order.total_qnt = total_qnt
		order.save
  	end
  	
  end

  def self.create_orders(params, create_by, inv)
  	invoice = "INV" + Time.now.strftime("%Y%m%d%H%M%S").to_s

  	orders = JSON.parse Base64.decode64(params[:orders])

  	order = Order.create!(
  		created_by: create_by,
  		order_status: 0,
  		invoice: invoice,
  		down_payment: params[:pembayaran].eql?("lunas") ? 0 : :params[:dp],
  		customer_id: params[:customer].to_i,
  		token: inv,
  		payment_status: params[:pembayaran].eql?("lunas") ? "lunas" : "dp"
  	)

  	if order
  		total = 0
	  	total_qnt = 0
	  	product = {}
		orders.each do |hash|
		  total_qnt += hash[1]["qnt"].to_i
		  total += (hash[1]["price"].to_i * hash[1]["qnt"].to_i)

		  item = ProductSize.find_by(id: hash[0].to_i)
		  if !order.order_items.find_by(product_id: item.product_id).present?
		  	order_item = order.order_items.create!(
				product_id: item.product_id,
				row_status: 1,
				sub_total: (hash[1]["price"].to_f * hash[1]["qnt"].to_i),
				production_status: :pending
			 )
		  	product[order_item.id] = {qnt: hash[1]["qnt"].to_i, price: hash[1]["price"].to_f}
		  else
		  	order_item = order.order_items.find_by(product_id: item.product_id)
		  	product[order_item.id][:qnt] += hash[1]["qnt"].to_i
		  end

		  order_item.order_item_sizes.create(
		  	price_per_item: hash[1]["price"].to_i,
		  	quantity: hash[1]["qnt"].to_i,
		  	product_size_id: item.id
		  )
		end

		product.each do |item|
			i = OrderItem.find_by(id: item.first)
			i.quantity = item[1][:qnt]
			i.sub_total = item[1][:price] * item[1][:qnt]
			i.save
		end

		order.total = total
		order.total_qnt = total_qnt
		order.save
  	end

  end

  def get_material
  	materials = {}

  	self.order_items.each do |items|
  		items.product.product_materials.each do |p_mat|
  			if materials.key?(p_mat.material_id)
  				materials[p_mat.material_id] = {
	  				qnt: materials[p_mat.material_id][:qnt] + (p_mat.use_per_unit * items.quantity),
	  				name: p_mat.material.name,
	  				unit: p_mat.material.unit
	  			}
  			else
  				materials[p_mat.material_id] = {
	  				qnt: (p_mat.use_per_unit * items.quantity),
	  				name: p_mat.material.name,
	  				unit: p_mat.material.unit
	  			}
  			end
  		end
  	end

  	return materials
  end

  def update_finish_production(params)
  	if params[:status].downcase.eql?("paid") or params[:status].downcase.eql?("sending")
  		
  		if params[:image].present?
			image = params[:image]
		  	encoded_image = Base64.encode64 image.read
		  	io = AvatarStringIO.new(Base64.decode64(encoded_image))

		  	image = MiniMagick::Image.open(image.path)

		  	valid_image = image.valid? rescue false
		else
		 	return false, "Image must be included"
		end

		if params[:status].downcase.eql?("paid")
			update = self.update(
				order_status: :paid,
				payment_image: io
			)
		else
			if !params[:kurir].downcase.eql?("internal") and !params[:shipping_invoice].present?
				return false, "Insert Shipping Invoice on external courire!"
			end

			si = params[:shipping_invoice]
			if params[:kurir].downcase.eql?("internal")
				no = Order.where(kurir: :internal).count + 1
				si = "INT-#{no}-#{Time.now.strftime("%Y%m%d%H%M%S").to_s}"
			end
			

			update = self.update(
				order_status: :sending,
				kurir: params[:kurir],
				shipping_invoice: si,
				shipping_invoice_image: io
			)
		end
		

		if update
			return true, "Status Updated!"
		else
			return false, "Internal error occured"
		end
		
	elsif params[:status].downcase.eql?("success")
		
		update = self.update(
			order_status: :success
		)

		if update
			return true, "Status Updated!"
		else
			return false, "Internal error occured"
		end
  	end
  	
  end

end
