# == Schema Information
#
# Table name: material_procurements
#
#  id           :bigint           not null, primary key
#  is_change    :boolean
#  request_date :datetime
#  review_by    :integer
#  row_status   :integer          default(1)
#  status       :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  user_id      :bigint
#
# Indexes
#
#  index_material_procurements_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id) ON DELETE => cascade
#
class MaterialProcurement < ApplicationRecord
  has_many :material_procurement_items
  belongs_to :user, optional: true

  scope :active, -> { where(row_status: 1) }

  enum status: [ :pending, :approve, :rejected, :done ]

  attr_reader :reviewer, :expected_total

  def self.newest
    self.order(created_at: :desc)
  end

  def self.by_status(status, condition)
    if condition.eql?("first")
      return self.where(status: status).first
    else
      return self.where(status: status).all
    end
  end

  def reviewer
    return User.find_by(id: self.review_by)
  end

  def expected_total
    return {
      material: self.material_procurement_items.count,
      total: self.material_procurement_items.map{|a| a.common_price}.sum,
      quantity: self.material_procurement_items.sum(:quantity)
    }
  end

  def self.create_request(params, current_user)
    get_last = MaterialProcurement.where(status: :pending)
    if get_last.present?
      return false
    else
      procurement = MaterialProcurement.create!(
        request_date: Time.now,
        review_by: User.find_by(role: "owner").email,
        status: :pending,
        is_change: false,
        user_id: current_user.id
      )

      if procurement
        items = JSON.parse Base64.decode64(params[:items])

        items.each do |item|
          procurement.material_procurement_items.create!(
            material_id: item.first,
            quantity: item.second
          )
        end
      end
    end
  end

  def update_request(params, current_user)

    case params[:status].to_s
    when "tolak"
      status = :rejected
    when "setuju"
      status = :approve
    else
      return false
    end

    is_change = false
    self.material_procurement_items.each_with_index do |item, i|
      if item.quantity != params[:quantity][i].to_i
        item.update!(quantity: params[:quantity][i].to_i)
        is_change = true
      end
    end

    request = self.update!(
      review_by: current_user.id,
      status: status,
      is_change: is_change
    )
  end
end
