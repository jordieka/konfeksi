# == Schema Information
#
# Table name: addresses
#
#  id           :bigint           not null, primary key
#  full_address :text
#  row_status   :integer          default(1)
#  target       :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  refer_id     :integer
#

class Address < ApplicationRecord
end
