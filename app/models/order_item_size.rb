# == Schema Information
#
# Table name: order_item_sizes
#
#  id              :bigint           not null, primary key
#  price_per_item  :float
#  quantity        :integer
#  row_status      :integer          default(1)
#  sub_total       :float
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  order_item_id   :bigint
#  product_size_id :bigint
#
# Indexes
#
#  index_order_item_sizes_on_order_item_id    (order_item_id)
#  index_order_item_sizes_on_product_size_id  (product_size_id)
#
# Foreign Keys
#
#  fk_rails_...  (order_item_id => order_items.id) ON DELETE => cascade
#  fk_rails_...  (product_size_id => product_sizes.id) ON DELETE => cascade
#
class OrderItemSize < ApplicationRecord

	belongs_to :order_item, optional: true
	belongs_to :product_size, optional: true
	has_many   :production_logs
end
