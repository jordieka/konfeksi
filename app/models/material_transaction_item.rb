# == Schema Information
#
# Table name: material_transaction_items
#
#  id                      :bigint           not null, primary key
#  quantity                :float
#  row_status              :integer          default(1)
#  sub_total               :float
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  material_id             :bigint
#  material_transaction_id :bigint
#  supplier_id             :bigint
#
# Indexes
#
#  index_material_transaction_items_on_material_id              (material_id)
#  index_material_transaction_items_on_material_transaction_id  (material_transaction_id)
#  index_material_transaction_items_on_supplier_id              (supplier_id)
#
# Foreign Keys
#
#  fk_rails_...  (material_id => materials.id) ON DELETE => cascade
#  fk_rails_...  (material_transaction_id => material_transactions.id) ON DELETE => cascade
#  fk_rails_...  (supplier_id => suppliers.id) ON DELETE => cascade
#

class MaterialTransactionItem < ApplicationRecord

  scope :active, -> { where(row_status: 1) }

  belongs_to :material_transaction, optional: true
  belongs_to :supplier, optional: true
  belongs_to :material, optional: true

  def self.get_totals
  	self.joins(:material_transaction).group("material_transaction_items.supplier_id").where("material_transactions = ?", :done).size
  end
end
