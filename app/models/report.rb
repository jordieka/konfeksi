# == Schema Information
#
# Table name: reports
#
#  id         :bigint           not null, primary key
#  is_read    :boolean          default(FALSE)
#  read_date  :datetime
#  report     :text
#  row_status :integer          default(1)
#  subject    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint
#
# Indexes
#
#  index_reports_on_user_id  (user_id)
#
class Report < ApplicationRecord
    belongs_to :user, optional: true
    scope :newest, -> { order(created_at: :desc) }

    after_create :new_report_mail

    def self.create_report params, user
        report = Report.create!(
            subject: params[:subject],
            report: params[:report].to_a.first,
            user_id: user.id.to_i
        )

        if report
            return true
        else
            return false
        end
        
    end

    def new_report_mail
        Rails.logger.info "============= mail sent!"
        return true
    end
    
end
