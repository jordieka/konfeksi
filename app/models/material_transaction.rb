# == Schema Information
#
# Table name: material_transactions
#
#  id                      :bigint           not null, primary key
#  bill_image              :text
#  finish_date             :datetime
#  invoice                 :string
#  order_date              :datetime
#  order_status            :integer
#  payment_date            :datetime
#  payment_image           :text
#  row_status              :integer          default(1)
#  total                   :float
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  material_procurement_id :bigint
#
# Indexes
#
#  index_material_transactions_on_material_procurement_id  (material_procurement_id)
#
# Foreign Keys
#
#  fk_rails_...  (material_procurement_id => material_procurements.id) ON DELETE => cascade
#

class MaterialTransaction < ApplicationRecord
  attr_accessor :bill_image, :payment_image
  mount_uploader :bill_image, ImageUploader
  mount_uploader :payment_image, ImageUploader

  scope :active, -> { where(row_status: 1) }	
  scope :complete, -> { where(order_status: :done) }  

  enum order_status: [ :pending, :payment_pending, :paid, :done ]

  has_many :material_transaction_items
  belongs_to :material_procurement, optional: true

  def self.newest
    self.order(created_at: :desc)
  end

  def self.by_status(status, condition)
    if condition.eql?("first")
      return self.where(order_status: status).first
    else
      return self.where(order_status: status).all
    end
  end

  def self.create_transaction(params, current_user)
  	request = MaterialProcurement.active.by_status(:approve, "first")

  	total = 0.0
  	items = []

  	request.material_procurement_items.each_with_index do |item, i|
  		if item.quantity < params[:quantities][i].to_i
  			return false, "Jumlah yang dimasukkan melebihi jumlah permintaan."
  		else
  			total += params[:prices][i].to_f
  			items.push(item.material_id.to_i)
  		end
  	end
  	
  	t = MaterialTransaction.new
	
  	t.invoice = "INV" + Time.now.strftime("%Y%m%d%H%M%S").to_s
  	t.order_date = Time.now
  	t.order_status = :payment_pending
  	t.total = total
  	t.material_procurement_id = request.id

  	# file upload
  	image = params[:bill_image]
  	encoded_image = Base64.encode64 image.read
  	io = AvatarStringIO.new(Base64.decode64(encoded_image))
  	t.bill_image = io

  	if t.save!
  		items.each_with_index do |item, i|
  			t.material_transaction_items.create!(
  				quantity: params[:quantities][i].to_i,
  				sub_total: params[:prices][i].to_f,
  				material_id: item,
  				supplier_id: params[:suppliers][i].to_i
  			)
  		end

  		return true, true
  	else
  		return false, "Terjadi kesalahan, silahkan coba lagi!"
  	end

  end

  def self.update_status(params, current_user)
  	if params[:invoice].to_s.eql?(params[:inv].to_s)
  		invoice = params[:invoice].to_s
  	else
  		invoice = params[:inv].to_s
  	end

  	transaction = MaterialTransaction.find_by(invoice: invoice) || MaterialTransaction.find_by("status != ?", :done) 
    
  	if transaction.present?
  		if transaction.order_status.eql?("pending")
  			transaction.order_status = :payment_pending
  		elsif transaction.order_status.eql?("payment_pending")
        image = params[:payment_image]
        encoded_image = Base64.encode64 image.read
        io = AvatarStringIO.new(Base64.decode64(encoded_image))
			  transaction.payment_image = io
			  transaction.payment_date = Time.now
  			transaction.order_status = :paid
  		elsif transaction.order_status.eql?("paid")
			  transaction.finish_date = Time.now
  			transaction.order_status = :done
  		else
  			return false, "Terjadi kesalahan dalam perubahan data"
  		end

  		if transaction.save!
        update_stock(transaction) if transaction.order_status.eql?("done")
        transaction.material_procurement.update(status: :done)
  			return true, "Status berhasil dirubah"
  		end
  	else
  		return false, "Data tidak ada"
  	end
  end

  def self.update_stock(transaction)
    transaction.material_transaction_items.each do |item|
      new_stock = item.material.stock + item.quantity
      item.material.stock = new_stock
      item.material.save!
    end

    return true
  end
end
