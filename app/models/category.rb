# == Schema Information
#
# Table name: categories
#
#  id         :bigint           not null, primary key
#  name       :string
#  row_status :integer          default(1)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Category < ApplicationRecord

  scope :active, -> { where(row_status: 1) }
  has_many :materials
  has_many :products
  
end
