# == Schema Information
#
# Table name: suppliers
#
#  id           :bigint           not null, primary key
#  address      :text
#  name         :string
#  phone_number :string
#  row_status   :integer          default(1)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Supplier < ApplicationRecord

  scope :active, -> { where(row_status: 1) }

  has_many :material_transaction_items
  has_many :material_transactions, through: :material_transaction_items
  
end
