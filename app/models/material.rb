# == Schema Information
#
# Table name: materials
#
#  id           :bigint           not null, primary key
#  common_price :float
#  material_use :integer
#  name         :string
#  row_status   :integer          default(1)
#  stock        :float            default(0.0)
#  unit         :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  category_id  :bigint
#
# Indexes
#
#  index_materials_on_category_id  (category_id)
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id) ON DELETE => cascade
#

class Material < ApplicationRecord

  enum material_use: [ :kain, :kancing, :resleting, :benang, :tali ]
  enum unit: [ :centimeter, :meter, :gram, :kilogram, :pcs, :lusin, :gulung ]

  scope :active, -> { where(row_status: 1) }

  has_many :material_request_items
  has_many :product_materials
  belongs_to :category, optional: true


end
