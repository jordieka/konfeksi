# == Schema Information
#
# Table name: order_items
#
#  id                  :bigint           not null, primary key
#  bordir_image        :text
#  bordir_position     :integer
#  bordir_price        :float
#  is_bordir           :boolean          default(FALSE)
#  is_ready            :boolean          default(FALSE)
#  is_sablon           :boolean
#  pattern_description :text
#  pattern_image       :text
#  production_status   :integer
#  quantity            :integer
#  row_status          :integer
#  sablon_image        :text
#  sablon_position     :integer
#  sablon_price        :float
#  sub_total           :float
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  order_id            :bigint
#  product_id          :bigint
#
# Indexes
#
#  index_order_items_on_order_id    (order_id)
#  index_order_items_on_product_id  (product_id)
#
# Foreign Keys
#
#  fk_rails_...  (order_id => orders.id) ON DELETE => cascade
#  fk_rails_...  (product_id => products.id) ON DELETE => cascade
#

class OrderItem < ApplicationRecord
	mount_uploader :pattern_image, ImageUploader
  	mount_uploader :bordir_image, ImageUploader
  	mount_uploader :sablon_image, ImageUploader
	
	enum production_status: [ :pending, :on_progress, :done ]
	enum bordir_position: [ :front, :back, :both ], _prefix: true
	enum sablon_position: [ :front, :back, :both ], _suffix: true

	belongs_to :order
	belongs_to :product, optional: true
	has_many   :order_item_sizes
	has_many   :production_logs, through: :order_item_sizes

	def update_order(params)

		additional_price = 0

		if self.is_bordir == true

			if params[:bordir_position].blank? || params[:bordir_price].blank?
				Rails.logger.info "============== Bordir"
				return false
			else
				self.bordir_position = params[:bordir_position]
				self.bordir_price = params[:bordir_price].to_f

				additional_price += (params[:bordir_price].to_f * self.quantity)

				if params[:bordir_image].blank? && self.bordir_image.blank?
					Rails.logger.info "============== Bordir image kosong"
					return false
				elsif !params[:bordir_image].blank?
					image = params[:bordir_image]
					begin
						encoded_image = Base64.encode64 image.read
						io = AvatarStringIO.new(Base64.decode64(encoded_image))
						v_image = MiniMagick::Image.open(image.path)
						if !v_image.type
				          return false, "File did not supported"
				        end
					rescue => error
					  Rails.logger.info "============== Bordir Image"
					  return false
					end
					
					self.bordir_image = io
				end
			end
		end

		Rails.logger.info "true : #{self.is_sablon == true}"

		if self.is_sablon == true
			if params[:sablon_position].blank? || params[:sablon_price].blank?
				Rails.logger.info "============== Sablon"
				return false
			else
				self.sablon_position = params[:sablon_position]
				self.sablon_price = params[:sablon_price].to_f

				additional_price += (params[:sablon_price].to_f * self.quantity)

				if params[:sablon_image].blank? && self.sablon_image.blank?
					Rails.logger.info "============== Sablon image kosong"
					return false
				elsif !params[:sablon_image].blank?
					image = params[:sablon_image]
					begin
						encoded_image = Base64.encode64 image.read
						io = AvatarStringIO.new(Base64.decode64(encoded_image))
						v_image = MiniMagick::Image.open(image.path)
						if !v_image.type
				          return false, "File did not supported"
				        end
					rescue => error
					  Rails.logger.info "============== Sablon Image"
					  return false
					end
					
					self.sablon_image = io
				end
			end
		end

		if params[:pattern_image].present?
			image = params[:pattern_image]
			begin
		    	encoded_image = Base64.encode64 image.read
		    	io = AvatarStringIO.new(Base64.decode64(encoded_image))
			rescue => error
			  return false
			end

			image = MiniMagick::Image.open(image.path)

	  		valid_image = image.valid? rescue false

	  		if valid_image == false
	  			return false
	  		end

	    	self.pattern_image = io	
	    	self.pattern_description = params[:description].first
	    elsif self.pattern_image.present?
	    	self.pattern_description = params[:description].first
		end
		
		self.is_ready = true
		
		if self.save
			order = self.order
			order.total += additional_price
			order.save!
		end

	end
end
