# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    alias_action :create, :read, :update, :destroy, to: :crud

    if user.role.eql?("owner")
      can :manage, :all
      can :manage, :rails_admin
      can :manage, :web_admin
    elsif user.role.eql?("superadmin")
      can :access, :rails_admin   # grant access to rails_admin
      can :read, :dashboard       # grant access to the dashboard
      can :read, :all
    elsif user.role.eql?("sekretaris")
      can [:create, :read, :add, :edit, :update], [Customer, Personnel, Order, OrderItem, OrderItemMaterial, Supplier]
      can :manage, [ Material, User, Personnel, Category, Product, Order, OrderItem, OrderItemMaterial ]
      can :read, [ProductionLog, ProductionOrder, Report, MaterialProcurement, MaterialProcurementItem, MaterialTransaction, MaterialTransactionItem, MaterialRequest, MaterialRequestItem  ]
      # can [:crud], [Supplier, Product, Order, OrderItem, OrderItemDetail, OrderItemMaterial, Product]
    elsif user.role.eql?("pengawas_produksi")
      can [:create, :read], [Report, ProductionLog, MaterialRequest, MaterialRequestItem]
      can [:read], [Product, ProductionOrder, Order, OrderItem, OrderItemMaterial, Material]
    elsif user.role.eql?("pengawas_konfeksi")
      can [:create], [Report, MaterialProcurement, MaterialProcurementItem, MaterialTransaction, MaterialTransactionItem ]
      can [:read, :update], [  Material, MaterialRequest, MaterialRequestItem, MaterialTransaction, MaterialTransactionItem, MaterialProcurement, MaterialProcurementItem ]
      can [:read], [Supplier]
    else
      can :read, :all
    end

    can [:read, :update], User, id: user.id 
    can [:read, :update], Personnel, user_id: user.id 
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
