# == Schema Information
#
# Table name: absents
#
#  id           :bigint           not null, primary key
#  description  :text
#  extra_hour   :integer
#  status       :integer          default("attend")
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  personnel_id :bigint
#  user_id      :bigint
#
# Indexes
#
#  index_absents_on_personnel_id  (personnel_id)
#  index_absents_on_user_id       (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (personnel_id => personnels.id)
#  fk_rails_...  (user_id => users.id)
#
class Absent < ApplicationRecord
  belongs_to :personnel
  belongs_to :user, optional: true

  enum status: [:attend, :absent, :paid_leave, :sick_leave]
end
