# == Schema Information
#
# Table name: order_item_materials
#
#  id                :bigint           not null, primary key
#  base_price        :float
#  quantity_expected :integer
#  quantity_used     :integer
#  row_status        :integer          default(1)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  materials_id      :bigint
#  order_items_id    :bigint
#
# Indexes
#
#  index_order_item_materials_on_materials_id    (materials_id)
#  index_order_item_materials_on_order_items_id  (order_items_id)
#

class OrderItemMaterial < ApplicationRecord
end
