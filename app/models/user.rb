# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  last_login             :datetime
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  role                   :integer
#  row_status             :integer          default(1)
#  temp_password          :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  enum role: [ :owner, :superadmin, :sekretaris, :pengawas_produksi, :pengawas_konfeksi]
  scope :active, -> { where(row_status: 1) }

  has_one  :personnel
  has_many :orders, foreign_key: "created_by"
  has_many :absents
  has_many :reports

  after_create :send_admin_mail

  def send_admin_mail
    UserMailer.new_user(self).deliver_later!
  end

  def update_personnel_data(params)
    
    self.personnel.full_name = params[:full_name] 
    self.personnel.email = params[:email] 
    self.personnel.gender =  params[:gender]
    self.personnel.phone_number =  params[:phone_number]
    self.personnel.birth_date =  params[:birth_date]

    if self.personnel.save!
      self.email = params[:email]
      self.save!
    else
      return false
    end
  end

  def update_password(params)
    Rails.logger.info "#{self.valid_password?(params[:old_password])}"
    if self.valid_password?(params[:old_password])
      self.password = params[:new_password]
      self.password_confirmation = params[:confirm_password]
      if self.save!
        return true, "done"
      else
        return false, "confirm"
      end
    else
      return false, "old"
    end
  end
end
