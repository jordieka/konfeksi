# == Schema Information
#
# Table name: products
#
#  id            :bigint           not null, primary key
#  base_price    :float
#  gender        :integer
#  margin_price  :float
#  name          :string
#  pattern_image :text
#  row_status    :integer          default(1)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  category_id   :bigint
#  user_id       :bigint
#
# Indexes
#
#  index_products_on_category_id  (category_id)
#  index_products_on_user_id      (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id) ON DELETE => cascade
#  fk_rails_...  (user_id => users.id)
#

require "mini_magick"

class Product < ApplicationRecord
  attr_accessor :pattern_image
  mount_uploader :pattern_image, ImageUploader

  scope :active, -> { where(row_status: 1) }

  enum gender: [:universal, :female, :male]

  belongs_to :user
  belongs_to :category
  has_many :product_sizes
  has_many :product_materials
  has_many :order_items

  def self.has_product_materials
    self.where(id: ProductMaterial.select(:product_id))
  end

  def self.create_product(params, user_id)
    product = Product.new
  	product.name = params[:name]
  	product.base_price =  params[:base_price]
  	product.margin_price =  params[:margin_price]
  	product.gender = params[:gender]
  	product.category_id = params[:category].to_i
  	product.user_id = user_id

    image = params[:pattern_image]
    begin
      encoded_image = Base64.encode64 image.read
      io = AvatarStringIO.new(Base64.decode64(encoded_image))

      v_image = MiniMagick::Image.open(image.path)

      if !v_image.type
        return false, "File did not supported"
      end
    rescue => error
      Rails.logger.info "============== Pattern Image"
      return false, "File did not supported"
    end

    product.pattern_image = io

    sizes = JSON.parse Base64.decode64(params[:sizes])

    if sizes.empty?
      return false, "Item has not been inputed"
    end

  	if product.save!
	  	sizes.each_with_index do |size, i|
	  		product.product_sizes.create!(
	  			size: size,
	  			margin_price: params[:margins][i].nil? ? 0 : params[:margins][i].to_f
	  		)

        return true, "Produk created!"
	  	end
    else
		  return false, "Failed to input data"
    end

  end

  def edit_product(params, user_id)
  	
  	self.name = params[:name]
  	self.base_price =  params[:base_price]
  	self.margin_price =  params[:margin_price]
  	self.gender = params[:gender]
  	self.category_id = params[:category].to_i

    if params[:pattern_image].present?
      image = params[:pattern_image]
      begin
        encoded_image = Base64.encode64 image.read
        io = AvatarStringIO.new(Base64.decode64(encoded_image))

        v_image = MiniMagick::Image.open(image.path)

        if !v_image.type
          return false, "File did not supported"
        end
      rescue => error
        Rails.logger.info "============== Pattern Image"
        return false, "File did not supported"
      end

      self.pattern_image = io
    end
    

  	sizes = JSON.parse Base64.decode64(params[:sizes])

    if sizes.empty?
      return false, "Item has not been inputed"
    end
    
  	if self.save
  		list = self.product_sizes.active.pluck(:size) - sizes

  		self.product_sizes.active.where(size: list).update_all(row_status: 0)

  	  	sizes.each_with_index do |size, i|
  	  		check_size = self.product_sizes.find_by(size: size)
  	  		if check_size.present?
  	  			check_size.update(margin_price: params[:margins][i].to_f)
  	  		else
  	  			self.product_sizes.create!(
  		  			size: size,
  		  			margin_price: params[:margins][i].to_f
  		  		)
  	  		end
  	  	end

      return true, "Produk updated!"
  	else
  		return false, "Failed to update!"
  	end
  end

  def add_material(params)
  	items = JSON.parse Base64.decode64(params[:items])

  	listed = []
  	items.each_with_index do |item, i|
  		material = self.product_materials.find_by(material_id: item.first.to_i)
  		if material.present?
  			material.use_per_unit = item.second.to_i
  			material.row_status = 1
  			material.save
  		else
  			self.product_materials.create!(
  				material_id: item.first.to_i,
  				use_per_unit: item.second.to_i,
  				row_status: 1
  			)
  		end
  		listed.push(item.first.to_i)
  	end

    Rails.logger.info("=================== #{ listed}") 

  	unlisted = self.product_materials.active.pluck(:material_id) - listed

    Rails.logger.info("=================== #{ self.product_materials.active.pluck(:material_id)}") 
    Rails.logger.info("=================== #{ unlisted}") 
    

  	if unlisted.empty?
  		return true
  	else
  		if self.product_materials.active.where(material_id: unlisted).update_all(row_status: 0)
  			return true
  		else 
  			return false
  		end
  	end
  end

end
