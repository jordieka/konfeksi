# == Schema Information
#
# Table name: product_procurements
#
#  id           :bigint           not null, primary key
#  request_date :datetime
#  row_status   :integer          default(1)
#  status       :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  users_id     :bigint
#
# Indexes
#
#  index_product_procurements_on_users_id  (users_id)
#

class ProductProcurement < ApplicationRecord
end
