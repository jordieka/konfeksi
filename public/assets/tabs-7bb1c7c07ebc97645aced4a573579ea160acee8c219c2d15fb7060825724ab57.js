$(document).ready(function(){
  $(".nav-tabs").on("click", "a", function (e) {
          e.preventDefault();
          if (!$(this).hasClass('add-contact')) {
              $(this).tab('show');
          }
      })
      .on("click", "span", function () {
          var anchor = $(this).siblings('a');
          $(anchor.attr('href')).remove();
          $(this).parent().remove();
          $(".nav-tabs li").children('a').first().click();
      });

  $('.add-contact').click(function (e) {
      e.preventDefault();
      var id = $(".nav-tabs").children().length; //think about it ;)
      alert()
      var tabId = 'contact_' + id;
      if (id <= 8) {
        $(this).closest('li').before('<li><a href="#contact_' + id + '">Material #'+id+'</a> <span> x </span></li>');
        $('.tab-content').append('<div class="tab-pane" id="' + tabId + '">Contact Form: New Contact ' + id + '</div>');
        $('.nav-tabs li:nth-child(' + id + ') a').click();
      }
  });
});
