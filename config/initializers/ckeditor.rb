Ckeditor.setup do |config|
  require 'ckeditor/orm/active_record'
  config.image_file_types = %w(jpg jpeg png gif )
  config.js_config_url = 'ckeditor/config.js'
  config.cdn_url = "//cdn.ckeditor.com/4.7.0/standard/ckeditor.js"
end