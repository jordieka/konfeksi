# == Route Map
#
#                                          Prefix Verb   URI Pattern                                                                              Controller#Action
#                                     rails_admin        /admin                                                                                   RailsAdmin::Engine
#                                        ckeditor        /ckeditor                                                                                Ckeditor::Engine
#                               new_user_password GET    /users/password/new(.:format)                                                            devise/passwords#new
#                              edit_user_password GET    /users/password/edit(.:format)                                                           devise/passwords#edit
#                                   user_password PATCH  /users/password(.:format)                                                                devise/passwords#update
#                                                 PUT    /users/password(.:format)                                                                devise/passwords#update
#                                                 POST   /users/password(.:format)                                                                devise/passwords#create
#                        cancel_user_registration GET    /users/cancel(.:format)                                                                  devise/registrations#cancel
#                           new_user_registration GET    /users/sign_up(.:format)                                                                 devise/registrations#new
#                          edit_user_registration GET    /users/edit(.:format)                                                                    devise/registrations#edit
#                               user_registration PATCH  /users(.:format)                                                                         devise/registrations#update
#                                                 PUT    /users(.:format)                                                                         devise/registrations#update
#                                                 DELETE /users(.:format)                                                                         devise/registrations#destroy
#                                                 POST   /users(.:format)                                                                         devise/registrations#create
#                                            root GET    /                                                                                        home#index
#                                   access_denied GET    /access_denied(.:format)                                                                 home#access_denied
#                                     check_order POST   /check_order(.:format)                                                                   home#check_order
#                                new_user_session GET    /login(.:format)                                                                         devise/sessions#new
#                            user_forgot_password GET    /forgot_password(.:format)                                                               devise/passwords#new
#                            user_change_password GET    /forgot_password/change_password(.:format)                                               devise/passwords#edit
#                                    user_session POST   /login(.:format)                                                                         devise/sessions#create
#                            destroy_user_session DELETE /logout(.:format)                                                                        devise/sessions#destroy
#                                       web_admin GET    /web_admin(.:format)                                                                     web_admin/dashboard#index
#                             web_admin_dashboard GET    /web_admin/dashboard(.:format)                                                           web_admin/dashboard#index
#                               web_admin_profile GET    /web_admin/profile(.:format)                                                             web_admin/dashboard#profile
#                                web_admin_absent GET    /web_admin/absent(.:format)                                                              web_admin/dashboard#absent
#                        web_admin_update_profile POST   /web_admin/update-profile(.:format)                                                      web_admin/dashboard#update_profile
#                       web_admin_update_password POST   /web_admin/update-password(.:format)                                                     web_admin/dashboard#update_password
#                             web_admin_materials GET    /web_admin/materials(.:format)                                                           web_admin/material#index
#                         add_web_admin_materials GET    /web_admin/materials/add(.:format)                                                       web_admin/material#add
#                        edit_web_admin_materials GET    /web_admin/materials/edit/:id(.:format)                                                  web_admin/material#edit
#                 add_request_web_admin_materials GET    /web_admin/materials/request/add(.:format)                                               web_admin/material#request_material_add
#                edit_request_web_admin_materials GET    /web_admin/materials/request/edit/:id(.:format)                                          web_admin/material#request_material_edit
#                list_request_web_admin_materials GET    /web_admin/materials/request(.:format)                                                   web_admin/material#request_material_list
#              detail_request_web_admin_materials GET    /web_admin/materials/request/detail/:id(.:format)                                        web_admin/material#request_material_detail
#                      create_web_admin_materials POST   /web_admin/materials(.:format)                                                           web_admin/material#create
#                      update_web_admin_materials POST   /web_admin/materials/update/:id(.:format)                                                web_admin/material#update
#                      delete_web_admin_materials POST   /web_admin/materials/delete/:id(.:format)                                                web_admin/material#delete
#              create_request_web_admin_materials POST   /web_admin/materials/request/create(.:format)                                            web_admin/material#request_buy_create
#              update_request_web_admin_materials POST   /web_admin/materials/request/update/:id(.:format)                                        web_admin/material#request_buy_update
#                             web_admin_suppliers GET    /web_admin/suppliers(.:format)                                                           web_admin/supplier#index
#                         add_web_admin_suppliers GET    /web_admin/suppliers/add(.:format)                                                       web_admin/supplier#add
#                        edit_web_admin_suppliers GET    /web_admin/suppliers/edit/:id(.:format)                                                  web_admin/supplier#edit
#                      create_web_admin_suppliers POST   /web_admin/suppliers(.:format)                                                           web_admin/supplier#create
#                      update_web_admin_suppliers POST   /web_admin/suppliers/update/:id(.:format)                                                web_admin/supplier#update
#                      delete_web_admin_suppliers POST   /web_admin/suppliers/delete/:id(.:format)                                                web_admin/supplier#delete
#                             web_admin_employees GET    /web_admin/employees(.:format)                                                           web_admin/employee#index
#                         add_web_admin_employees GET    /web_admin/employees/add(.:format)                                                       web_admin/employee#add
#                salary_recap_web_admin_employees GET    /web_admin/employees/recap(.:format)                                                     web_admin/employee#salary_recap
#                        edit_web_admin_employees GET    /web_admin/employees/edit/:id(.:format)                                                  web_admin/employee#edit
#                      create_web_admin_employees POST   /web_admin/employees(.:format)                                                           web_admin/employee#create
#                      update_web_admin_employees POST   /web_admin/employees/update/:id(.:format)                                                web_admin/employee#update
#                      delete_web_admin_employees POST   /web_admin/employees/delete/:id(.:format)                                                web_admin/employee#delete
#                             web_admin_customers GET    /web_admin/customers(.:format)                                                           web_admin/customer#index
#                         add_web_admin_customers GET    /web_admin/customers/add(.:format)                                                       web_admin/customer#add
#                        edit_web_admin_customers GET    /web_admin/customers/edit/:id(.:format)                                                  web_admin/customer#edit
#                      detail_web_admin_customers GET    /web_admin/customers/detail/:id(.:format)                                                web_admin/customer#show
#                      create_web_admin_customers POST   /web_admin/customers(.:format)                                                           web_admin/customer#create
#                      update_web_admin_customers POST   /web_admin/customers/update/:id(.:format)                                                web_admin/customer#update
#                      delete_web_admin_customers POST   /web_admin/customers/delete/:id(.:format)                                                web_admin/customer#delete
#               add_request_web_admin_inventories GET    /web_admin/inventories/request/add(.:format)                                             web_admin/inventory#request_buy_add
#              edit_request_web_admin_inventories GET    /web_admin/inventories/request/edit/:id(.:format)                                        web_admin/inventory#request_buy_edit
#              list_request_web_admin_inventories GET    /web_admin/inventories/request(.:format)                                                 web_admin/inventory#request_buy_list
#            detail_request_web_admin_inventories GET    /web_admin/inventories/request/detail/:id(.:format)                                      web_admin/inventory#request_buy_detail
#           add_transaction_web_admin_inventories GET    /web_admin/inventories/transaction/add(.:format)                                         web_admin/inventory#transaction_add
#          list_transaction_web_admin_inventories GET    /web_admin/inventories/transaction(.:format)                                             web_admin/inventory#transaction_list
#        detail_transaction_web_admin_inventories GET    /web_admin/inventories/transaction/detail/:id(.:format)                                  web_admin/inventory#transaction_detail
#            create_request_web_admin_inventories POST   /web_admin/inventories/request/create(.:format)                                          web_admin/inventory#request_buy_create
#            update_request_web_admin_inventories POST   /web_admin/inventories/request/update/:id(.:format)                                      web_admin/inventory#request_buy_update
#        create_transaction_web_admin_inventories POST   /web_admin/inventories/transaction/create(.:format)                                      web_admin/inventory#transaction_create
# update_status_transaction_web_admin_inventories POST   /web_admin/inventories/transaction/update-status(.:format)                               web_admin/inventory#transaction_update_status
#                       list_web_admin_categories GET    /web_admin/categories(.:format)                                                          web_admin/category#index
#                     create_web_admin_categories POST   /web_admin/categories/create(.:format)                                                   web_admin/category#create
#                     update_web_admin_categories POST   /web_admin/categories/update(.:format)                                                   web_admin/category#update
#                     delete_web_admin_categories POST   /web_admin/categories/delete/:id(.:format)                                               web_admin/category#delete
#                         list_web_admin_products GET    /web_admin/products(.:format)                                                            web_admin/product#index
#                          add_web_admin_products GET    /web_admin/products/add(.:format)                                                        web_admin/product#add
#                         edit_web_admin_products GET    /web_admin/products/edit/:id(.:format)                                                   web_admin/product#edit
#                       detail_web_admin_products GET    /web_admin/products/:id(.:format)                                                        web_admin/product#show
#                     material_web_admin_products GET    /web_admin/products/material/:id(.:format)                                               web_admin/product#material
#                edit_material_web_admin_products GET    /web_admin/products/material/edit/:id(.:format)                                          web_admin/product#edit_material
#                  get_product_web_admin_products GET    /web_admin/products/get_product/:id(.:format)                                            web_admin/product#get_product_by_id
#                     get_size_web_admin_products GET    /web_admin/products/get_size/:id(.:format)                                               web_admin/product#get_size
#                       create_web_admin_products POST   /web_admin/products/create(.:format)                                                     web_admin/product#create
#                       update_web_admin_products POST   /web_admin/products/update/:id(.:format)                                                 web_admin/product#update
#                       delete_web_admin_products POST   /web_admin/products/delete/:id(.:format)                                                 web_admin/product#delete
#              create_material_web_admin_products POST   /web_admin/products/material/create(.:format)                                            web_admin/product#create_material
#              update_material_web_admin_products POST   /web_admin/products/material/update/:id(.:format)                                        web_admin/product#update_material
#                           list_web_admin_orders GET    /web_admin/orders(.:format)                                                              web_admin/order#index
#                            add_web_admin_orders GET    /web_admin/orders/add(.:format)                                                          web_admin/order#add
#                add_negotiation_web_admin_orders GET    /web_admin/orders/negotiation(.:format)                                                  web_admin/order#add_negotiation
#                         manage_web_admin_orders GET    /web_admin/orders/manage/:invoice(.:format)                                              web_admin/order#manage
#                           edit_web_admin_orders GET    /web_admin/orders/edit/:id(.:format)                                                     web_admin/order#edit
#                         detail_web_admin_orders GET    /web_admin/orders/detail/:id(.:format)                                                   web_admin/order#detail
#                     export_pdf_web_admin_orders GET    /web_admin/orders/export_pdf/:invoice(.:format)                                          web_admin/order#export_pdf
#                    order_items_web_admin_orders GET    /web_admin/orders/order_items/:invoice(.:format)                                         web_admin/order#order_items
#                         create_web_admin_orders POST   /web_admin/orders(.:format)                                                              web_admin/order#create
#             create_negotiation_web_admin_orders POST   /web_admin/orders/negotiation(.:format)                                                  web_admin/order#create_negotiation
#                  update_status_web_admin_orders POST   /web_admin/orders/status/:id(.:format)                                                   web_admin/order#update_status
#                         update_web_admin_orders POST   /web_admin/orders/update/:id(.:format)                                                   web_admin/order#update
#       update_finish_production_web_admin_orders POST   /web_admin/orders/update_finish_production(.:format)                                     web_admin/order#update_finish_production
#                         delete_web_admin_orders POST   /web_admin/orders/delete/:invoice(.:format)                                              web_admin/order#delete
#                      list_web_admin_productions GET    /web_admin/productions(.:format)                                                         web_admin/production#index
#                    detail_web_admin_productions GET    /web_admin/productions/:invoice(.:format)                                                web_admin/production#show
#                  progress_web_admin_productions GET    /web_admin/productions/progress/:invoice(.:format)                                       web_admin/production#create_progress
#                 get_sizes_web_admin_productions GET    /web_admin/productions/get_sizes/:id(.:format)                                           web_admin/production#get_sizes
#                 show_size_web_admin_productions GET    /web_admin/productions/show_size/:size(.:format)                                         web_admin/production#show_size
#                 show_logs_web_admin_productions GET    /web_admin/productions/show_logs/:invoice(.:format)                                      web_admin/production#show_logs
#                    create_web_admin_productions POST   /web_admin/productions(.:format)                                                         web_admin/production#create
#          start_production_web_admin_productions POST   /web_admin/productions/start_production(.:format)                                        web_admin/production#start_production
#                      employee_web_admin_reports GET    /web_admin/reports/employee(.:format)                                                    web_admin/report#employee
#              material_request_web_admin_reports GET    /web_admin/reports/material_request(.:format)                                            web_admin/report#material_request
#          material_transaction_web_admin_reports GET    /web_admin/reports/material_transaction(.:format)                                        web_admin/report#material_transaction
#                         order_web_admin_reports GET    /web_admin/reports/order(.:format)                                                       web_admin/report#order
#                        create_web_admin_reports GET    /web_admin/reports/create(.:format)                                                      web_admin/report#create
#                   user_report_web_admin_reports GET    /web_admin/reports/user-report(.:format)                                                 web_admin/report#user_report
#                          view_web_admin_reports GET    /web_admin/reports/view/:id(.:format)                                                    web_admin/report#view
#                   post_create_web_admin_reports POST   /web_admin/reports/create(.:format)                                                      web_admin/report#post_create
#                              rails_service_blob GET    /rails/active_storage/blobs/:signed_id/*filename(.:format)                               active_storage/blobs#show
#                       rails_blob_representation GET    /rails/active_storage/representations/:signed_blob_id/:variation_key/*filename(.:format) active_storage/representations#show
#                              rails_disk_service GET    /rails/active_storage/disk/:encoded_key/*filename(.:format)                              active_storage/disk#show
#                       update_rails_disk_service PUT    /rails/active_storage/disk/:encoded_token(.:format)                                      active_storage/disk#update
#                            rails_direct_uploads POST   /rails/active_storage/direct_uploads(.:format)                                           active_storage/direct_uploads#create
#
# Routes for RailsAdmin::Engine:
#   dashboard GET         /                                      rails_admin/main#dashboard
#       index GET|POST    /:model_name(.:format)                 rails_admin/main#index
#         new GET|POST    /:model_name/new(.:format)             rails_admin/main#new
#      export GET|POST    /:model_name/export(.:format)          rails_admin/main#export
# bulk_delete POST|DELETE /:model_name/bulk_delete(.:format)     rails_admin/main#bulk_delete
# bulk_action POST        /:model_name/bulk_action(.:format)     rails_admin/main#bulk_action
#        show GET         /:model_name/:id(.:format)             rails_admin/main#show
#        edit GET|PUT     /:model_name/:id/edit(.:format)        rails_admin/main#edit
#      delete GET|DELETE  /:model_name/:id/delete(.:format)      rails_admin/main#delete
# show_in_app GET         /:model_name/:id/show_in_app(.:format) rails_admin/main#show_in_app
#
# Routes for Ckeditor::Engine:
#         pictures GET    /pictures(.:format)             ckeditor/pictures#index
#                  POST   /pictures(.:format)             ckeditor/pictures#create
#          picture DELETE /pictures/:id(.:format)         ckeditor/pictures#destroy
# attachment_files GET    /attachment_files(.:format)     ckeditor/attachment_files#index
#                  POST   /attachment_files(.:format)     ckeditor/attachment_files#create
#  attachment_file DELETE /attachment_files/:id(.:format) ckeditor/attachment_files#destroy

Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users, skip: [:sessions]
  root to: "home#index"

  get "access_denied", to: "home#access_denied", as: "access_denied"
  post "check_order", to: "home#check_order", as: "check_order"

  as :user do
    get 'login', to: 'devise/sessions#new', as: :new_user_session
    get 'forgot_password', to: 'devise/passwords#new', as: :user_forgot_password
    get 'forgot_password/change_password', to: 'devise/passwords#edit', as: :user_change_password

    post 'login', to: 'devise/sessions#create', as: :user_session
    delete 'logout', to: 'devise/sessions#destroy', as: :destroy_user_session
  end

  namespace :web_admin do
      get '/', to: "dashboard#index"
      get '/dashboard', to: "dashboard#index"
      get '/profile', to: "dashboard#profile"
      get '/absent', to: "dashboard#absent"

      post '/update-profile', to: "dashboard#update_profile"
      post '/update-password', to: "dashboard#update_password"

      resource :materials, only:[] do
        get "/", to: "material#index"
        get "/add", to: "material#add"
        get "/edit/:id", to: "material#edit", as: "edit"

        get "/request/add/", to: "material#request_material_add", as: "add_request"
        get "/request/edit/:id", to: "material#request_material_edit", as: "edit_request"
        get "/request", to: "material#request_material_list", as: "list_request"
        get "/request/detail/:id", to: "material#request_material_detail", as: "detail_request"

        post "/", to: "material#create", as: "create"
        post "/update/:id", to: "material#update", as: "update"
        post "/delete/:id", to: "material#delete", as: "delete"

        post "/request/create", to: "material#request_buy_create", as: "create_request"
        post "/request/update/:id", to: "material#request_buy_update", as: "update_request"
      end

      resource :suppliers, only:[] do
        get "/", to: "supplier#index"
        get "/add", to: "supplier#add"
        get "/edit/:id", to: "supplier#edit", as: "edit"

        post "/", to: "supplier#create", as: "create"
        post "/update/:id", to: "supplier#update", as: "update"
        post "/delete/:id", to: "supplier#delete", as: "delete"
      end

      resource :employees, only:[] do
        get "/", to: "employee#index"
        get "/add", to: "employee#add"
        get "/recap", to: "employee#salary_recap", as: "salary_recap"
        get "/edit/:id", to: "employee#edit", as: "edit"

        post "/", to: "employee#create", as: "create"
        post "/update/:id", to: "employee#update", as: "update"
        post "/delete/:id", to: "employee#delete", as: "delete"
      end

      resource :customers, only:[] do
        get "/", to: "customer#index"
        get "/add", to: "customer#add"
        get "/edit/:id", to: "customer#edit", as: "edit"
        get "/detail/:id", to: "customer#show", as: "detail"

        post "/", to: "customer#create", as: "create"
        post "/update/:id", to: "customer#update", as: "update"
        post "/delete/:id", to: "customer#delete", as: "delete"
      end

      resource :inventories, only:[] do
        get "/request/add", to: "inventory#request_buy_add", as: "add_request"
        get "/request/edit/:id", to: "inventory#request_buy_edit", as: "edit_request"
        get "/request", to: "inventory#request_buy_list", as: "list_request"
        get "/request/detail/:id", to: "inventory#request_buy_detail", as: "detail_request"

        get "/transaction/add", to: "inventory#transaction_add", as: "add_transaction"
        get "/transaction", to: "inventory#transaction_list", as: "list_transaction"
        get "/transaction/detail/:id", to: "inventory#transaction_detail", as: "detail_transaction"

        post "/request/create", to: "inventory#request_buy_create", as: "create_request"
        post "/request/update/:id", to: "inventory#request_buy_update", as: "update_request"
        
        post "/transaction/create", to: "inventory#transaction_create", as: "create_transaction"
        post "/transaction/update-status", to: "inventory#transaction_update_status", as: "update_status_transaction"
      end

      resource :categories, only:[] do
        get "/", to: "category#index", as: "list"

        post "/create", to: "category#create", as: "create"
        post "/update", to: "category#update", as: "update"
        post "/delete/:id", to: "category#delete", as: "delete"
      end

      resource :products, only: [] do
        get "/", to: "product#index", as: "list"
        get "/add", to: "product#add", as: "add"
        get "/edit/:id", to: "product#edit", as: "edit"
        get "/:id", to: "product#show", as: "detail"

        get "/material/:id", to: "product#material", as: "material"
        get "/material/edit/:id", to: "product#edit_material", as: "edit_material"

        get "/get_product/:id", to: "product#get_product_by_id", as: "get_product"
        get "/get_size/:id", to: "product#get_size", as: "get_size"

        post "/create", to: "product#create", as: "create"
        post "/update/:id", to: "product#update", as: "update"
        post "/delete/:id", to: "product#delete", as: "delete"

        post "/material/create", to: "product#create_material", as: "create_material"
        post "/material/update/:id", to: "product#update_material", as: "update_material"
      end

      resource :orders, only: [] do
        get "/", to: "order#index", as: "list"
        get "add", to: "order#add", as: "add"
        get "negotiation", to: "order#add_negotiation", as: "add_negotiation"
        get "manage/:invoice", to: "order#manage", as: "manage"
        get "edit/:id", to: "order#edit", as: "edit"
        get "detail/:id", to: "order#detail", as: "detail"
        get "export_pdf/:invoice", to: "order#export_pdf", as: "export_pdf"

        get "/order_items/:invoice", to: "order#order_items", as: "order_items"

        post "/", to: "order#create", as: "create"
        post "/negotiation", to: "order#create_negotiation", as: "create_negotiation"
        post "/status/:id", to: "order#update_status", as: "update_status"
        post "/update/:id", to: "order#update", as: "update"
        post "/update_finish_production", to: "order#update_finish_production", as: "update_finish_production"
        post "/delete/:invoice", to: "order#delete", as: "delete"
      end

      resource :productions, only: [] do
        get "/", to: "production#index", as: "list"
        get "/:invoice", to: "production#show", as: "detail"
        get "/progress/:invoice", to: "production#create_progress", as: "progress"
        get "/get_sizes/:id", to: "production#get_sizes", as: "get_sizes"
        get "/show_size/:size", to: "production#show_size", as: "show_size"
        get "/show_logs/:invoice", to: "production#show_logs", as: "show_logs"

        post "/", to: "production#create", as: "create"
        post "/start_production", to: "production#start_production", as: "start_production"
      end

      resource :reports, only: [] do
        get "employee", to: "report#employee", as: "employee"
        get "material_request", to: "report#material_request", as: "material_request"
        get "material_transaction", to: "report#material_transaction", as: "material_transaction"
        get "order", to: "report#order", as: "order"
        get "create", to: "report#create", as: "create"
        get "user-report", to: "report#user_report", as: "user_report"
        get "view/:id", to: "report#view", as: "view"

        post "create", to: "report#post_create", as: "post_create"
      end
  end
end
