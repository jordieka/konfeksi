# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_08_25_095255) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "absents", force: :cascade do |t|
    t.bigint "personnel_id"
    t.bigint "user_id"
    t.integer "status", default: 0
    t.text "description"
    t.integer "extra_hour"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["personnel_id"], name: "index_absents_on_personnel_id"
    t.index ["user_id"], name: "index_absents_on_user_id"
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "addresses", force: :cascade do |t|
    t.integer "refer_id"
    t.integer "target"
    t.text "full_address"
    t.integer "row_status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.integer "row_status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customers", force: :cascade do |t|
    t.string "full_name"
    t.string "company_name"
    t.string "phone_number"
    t.integer "customer_type"
    t.integer "row_status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "id_card"
    t.string "email"
    t.string "company_phone"
    t.text "address"
  end

  create_table "material_procurement_items", force: :cascade do |t|
    t.bigint "material_procurement_id"
    t.bigint "material_id"
    t.float "quantity"
    t.integer "row_status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["material_id"], name: "index_material_procurement_items_on_material_id"
    t.index ["material_procurement_id"], name: "index_material_procurement_items_on_material_procurement_id"
  end

  create_table "material_procurements", force: :cascade do |t|
    t.datetime "request_date", default: -> { "CURRENT_TIMESTAMP" }
    t.bigint "user_id"
    t.integer "status"
    t.integer "row_status", default: 1
    t.boolean "is_change"
    t.integer "review_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_material_procurements_on_user_id"
  end

  create_table "material_request_items", force: :cascade do |t|
    t.bigint "material_requests_id"
    t.bigint "material_id"
    t.float "quantity"
    t.integer "row_status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_complete", default: false
    t.float "qnt_left"
    t.index ["material_id"], name: "index_material_request_items_on_material_id"
    t.index ["material_requests_id"], name: "index_material_request_items_on_material_requests_id"
  end

  create_table "material_requests", force: :cascade do |t|
    t.datetime "request_date", default: -> { "CURRENT_TIMESTAMP" }
    t.bigint "user_id"
    t.integer "status"
    t.boolean "is_change"
    t.integer "row_status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "review_by"
    t.bigint "order_id"
    t.index ["order_id"], name: "index_material_requests_on_order_id"
    t.index ["user_id"], name: "index_material_requests_on_user_id"
  end

  create_table "material_transaction_items", force: :cascade do |t|
    t.bigint "material_transaction_id"
    t.bigint "material_id"
    t.float "quantity"
    t.float "sub_total"
    t.integer "row_status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "supplier_id"
    t.index ["material_id"], name: "index_material_transaction_items_on_material_id"
    t.index ["material_transaction_id"], name: "index_material_transaction_items_on_material_transaction_id"
    t.index ["supplier_id"], name: "index_material_transaction_items_on_supplier_id"
  end

  create_table "material_transactions", force: :cascade do |t|
    t.string "invoice"
    t.datetime "order_date"
    t.datetime "payment_date"
    t.integer "order_status"
    t.datetime "finish_date"
    t.float "total"
    t.integer "row_status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "bill_image"
    t.text "payment_image"
    t.bigint "material_procurement_id"
    t.index ["material_procurement_id"], name: "index_material_transactions_on_material_procurement_id"
  end

  create_table "materials", force: :cascade do |t|
    t.string "name"
    t.float "stock", default: 0.0
    t.integer "material_use"
    t.integer "row_status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "common_price"
    t.integer "unit"
    t.bigint "category_id"
    t.index ["category_id"], name: "index_materials_on_category_id"
  end

  create_table "order_item_details", force: :cascade do |t|
    t.bigint "order_items_id"
    t.string "size"
    t.integer "quantity"
    t.integer "row_status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_items_id"], name: "index_order_item_details_on_order_items_id"
  end

  create_table "order_item_materials", force: :cascade do |t|
    t.bigint "materials_id"
    t.bigint "order_items_id"
    t.integer "quantity_expected"
    t.integer "quantity_used"
    t.float "base_price"
    t.integer "row_status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["materials_id"], name: "index_order_item_materials_on_materials_id"
    t.index ["order_items_id"], name: "index_order_item_materials_on_order_items_id"
  end

  create_table "order_item_sizes", force: :cascade do |t|
    t.bigint "order_item_id"
    t.bigint "product_size_id"
    t.integer "quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "price_per_item"
    t.integer "row_status", default: 1
    t.float "sub_total"
    t.index ["order_item_id"], name: "index_order_item_sizes_on_order_item_id"
    t.index ["product_size_id"], name: "index_order_item_sizes_on_product_size_id"
  end

  create_table "order_items", force: :cascade do |t|
    t.integer "quantity"
    t.integer "production_status"
    t.text "bordir_image"
    t.text "pattern_image"
    t.boolean "is_sablon"
    t.text "sablon_image"
    t.float "sub_total"
    t.integer "row_status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "order_id"
    t.bigint "product_id"
    t.boolean "is_ready", default: false
    t.boolean "is_bordir", default: false
    t.float "sablon_price"
    t.integer "sablon_position"
    t.float "bordir_price"
    t.integer "bordir_position"
    t.text "pattern_description"
    t.index ["order_id"], name: "index_order_items_on_order_id"
    t.index ["product_id"], name: "index_order_items_on_product_id"
  end

  create_table "orders", force: :cascade do |t|
    t.string "invoice", null: false
    t.bigint "customer_id"
    t.integer "order_status"
    t.integer "total_qnt"
    t.float "down_payment"
    t.float "total"
    t.integer "row_status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "created_by"
    t.string "token"
    t.string "shipping_invoice"
    t.text "shipping_invoice_image"
    t.text "payment_image"
    t.integer "kurir"
    t.integer "payment_status", default: 0
    t.index ["customer_id"], name: "index_orders_on_customer_id"
  end

  create_table "personnels", force: :cascade do |t|
    t.bigint "user_id"
    t.string "full_name"
    t.string "phone_number"
    t.datetime "birth_date"
    t.integer "gender"
    t.integer "position"
    t.integer "row_status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email"
    t.float "salary"
    t.integer "salary_type"
    t.integer "remaining_day_off", default: 12
    t.index ["user_id"], name: "index_personnels_on_user_id"
  end

  create_table "product_material_options", force: :cascade do |t|
    t.bigint "product_types_id"
    t.bigint "materials_id"
    t.integer "type"
    t.float "material_per_pcs"
    t.integer "unit"
    t.float "material_margin"
    t.integer "row_status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_required"
    t.index ["materials_id"], name: "index_product_material_options_on_materials_id"
    t.index ["product_types_id"], name: "index_product_material_options_on_product_types_id"
  end

  create_table "product_materials", force: :cascade do |t|
    t.bigint "product_id"
    t.bigint "material_id"
    t.float "use_per_unit"
    t.integer "row_status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["material_id"], name: "index_product_materials_on_material_id"
    t.index ["product_id"], name: "index_product_materials_on_product_id"
  end

  create_table "product_procurement_items", force: :cascade do |t|
    t.bigint "product_procurements_id"
    t.bigint "materials_id"
    t.integer "quantity"
    t.integer "row_status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["materials_id"], name: "index_product_procurement_items_on_materials_id"
    t.index ["product_procurements_id"], name: "index_product_procurement_items_on_product_procurements_id"
  end

  create_table "product_procurements", force: :cascade do |t|
    t.datetime "request_date", default: -> { "CURRENT_TIMESTAMP" }
    t.bigint "users_id"
    t.integer "status"
    t.integer "row_status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["users_id"], name: "index_product_procurements_on_users_id"
  end

  create_table "product_sizes", force: :cascade do |t|
    t.integer "size"
    t.float "margin_price"
    t.bigint "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "row_status", default: 1
    t.index ["product_id"], name: "index_product_sizes_on_product_id"
  end

  create_table "product_types", force: :cascade do |t|
    t.string "name"
    t.integer "category"
    t.integer "gender"
    t.float "base_price"
    t.float "margin_price"
    t.integer "row_status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "production_logs", force: :cascade do |t|
    t.integer "progress_status"
    t.integer "quantity_finish"
    t.text "description"
    t.integer "row_status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "quantity_left"
    t.bigint "order_item_size_id"
    t.text "image"
    t.index ["order_item_size_id"], name: "index_production_logs_on_order_item_size_id"
  end

  create_table "production_orders", force: :cascade do |t|
    t.bigint "order_id"
    t.integer "status"
    t.integer "order"
    t.integer "row_status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_id"], name: "index_production_orders_on_order_id"
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.bigint "category_id"
    t.integer "gender"
    t.float "base_price"
    t.float "margin_price"
    t.integer "row_status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.text "pattern_image"
    t.index ["category_id"], name: "index_products_on_category_id"
    t.index ["user_id"], name: "index_products_on_user_id"
  end

  create_table "reports", force: :cascade do |t|
    t.bigint "user_id"
    t.text "report"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "subject"
    t.boolean "is_read", default: false
    t.datetime "read_date"
    t.integer "row_status", default: 1
    t.index ["user_id"], name: "index_reports_on_user_id"
  end

  create_table "suppliers", force: :cascade do |t|
    t.string "name"
    t.string "phone_number"
    t.text "address"
    t.integer "row_status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "role"
    t.datetime "last_login", default: -> { "CURRENT_TIMESTAMP" }
    t.integer "row_status", default: 1
    t.string "temp_password"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "absents", "personnels"
  add_foreign_key "absents", "users"
  add_foreign_key "material_procurement_items", "material_procurements", on_delete: :cascade
  add_foreign_key "material_procurement_items", "materials", on_delete: :cascade
  add_foreign_key "material_procurements", "users", on_delete: :cascade
  add_foreign_key "material_request_items", "material_requests", column: "material_requests_id", on_delete: :cascade
  add_foreign_key "material_request_items", "materials", on_delete: :cascade
  add_foreign_key "material_requests", "orders", on_delete: :cascade
  add_foreign_key "material_requests", "users", on_delete: :cascade
  add_foreign_key "material_transaction_items", "material_transactions", on_delete: :cascade
  add_foreign_key "material_transaction_items", "materials", on_delete: :cascade
  add_foreign_key "material_transaction_items", "suppliers", on_delete: :cascade
  add_foreign_key "material_transactions", "material_procurements", on_delete: :cascade
  add_foreign_key "materials", "categories", on_delete: :cascade
  add_foreign_key "order_item_sizes", "order_items", on_delete: :cascade
  add_foreign_key "order_item_sizes", "product_sizes", on_delete: :cascade
  add_foreign_key "order_items", "orders", on_delete: :cascade
  add_foreign_key "order_items", "products", on_delete: :cascade
  add_foreign_key "orders", "customers", on_delete: :cascade
  add_foreign_key "orders", "users", column: "created_by", on_delete: :cascade
  add_foreign_key "personnels", "users", on_delete: :cascade
  add_foreign_key "product_materials", "materials", on_delete: :cascade
  add_foreign_key "product_materials", "products", on_delete: :cascade
  add_foreign_key "product_sizes", "products"
  add_foreign_key "production_logs", "order_item_sizes", on_delete: :cascade
  add_foreign_key "products", "categories", on_delete: :cascade
  add_foreign_key "products", "users"
end
