class AddFieldToProductMaterialOptions < ActiveRecord::Migration[5.2]
  def change
    add_column :product_material_options, :is_required, :boolean
  end
end
