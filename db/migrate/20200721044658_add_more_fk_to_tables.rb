class AddMoreFkToTables < ActiveRecord::Migration[5.2]
  DOWNTIME = false

  def up
    # safe to use: it requires short lock on the table since we don't validate the foreign key
    add_foreign_key :orders, :customers, on_delete: :cascade, validate: false
    add_foreign_key :material_request_items, :materials, on_delete: :cascade, validate: false
    add_foreign_key :product_materials, :materials, on_delete: :cascade, validate: false
    add_foreign_key :material_procurements, :users, on_delete: :cascade, validate: false
    add_foreign_key :material_procurement_items, :material_procurements, on_delete: :cascade, validate: false
    add_foreign_key :material_procurement_items, :materials, on_delete: :cascade, validate: false
    add_foreign_key :material_request_items, :material_requests, column: :material_requests_id, on_delete: :cascade, validate: false
    add_foreign_key :material_requests, :orders, on_delete: :cascade, validate: false
    add_foreign_key :material_requests, :users, on_delete: :cascade, validate: false
    add_foreign_key :material_transactions, :material_procurements, on_delete: :cascade, validate: false
    add_foreign_key :material_transaction_items, :material_transactions, on_delete: :cascade, validate: false
    add_foreign_key :material_transaction_items, :suppliers, on_delete: :cascade, validate: false
    add_foreign_key :material_transaction_items, :materials, on_delete: :cascade, validate: false
    add_foreign_key :orders, :users, column: :created_by, on_delete: :cascade, validate: false
    add_foreign_key :order_items, :orders, on_delete: :cascade, validate: false
    add_foreign_key :order_items, :products, on_delete: :cascade, validate: false
    add_foreign_key :order_item_sizes, :order_items, on_delete: :cascade, validate: false
    add_foreign_key :order_item_sizes, :product_sizes, on_delete: :cascade, validate: false
    add_foreign_key :production_logs, :order_item_sizes, on_delete: :cascade, validate: false
    add_foreign_key :personnels, :users, on_delete: :cascade, validate: false
    add_foreign_key :product_materials, :products, on_delete: :cascade, validate: false
  end

  def down
    remove_foreign_key_if_exists :orders, column: :customer_id
    remove_foreign_key_if_exists :material_request_items, column: :material_id
    remove_foreign_key_if_exists :product_materials, column: :material_id
    remove_foreign_key_if_exists :material_procurements, column: :user_id
    remove_foreign_key_if_exists :material_procurement_items, column: :material_procurement_id
    remove_foreign_key_if_exists :material_procurement_items, column: :material_id
    remove_foreign_key_if_exists :material_request_items, column: :material_request_id
    remove_foreign_key_if_exists :material_requests, column: :order_id
    remove_foreign_key_if_exists :material_requests, column: :user_id
    remove_foreign_key_if_exists :material_transactions, column: :material_procurement_id
    remove_foreign_key_if_exists :material_transaction_items, column: :material_id
    remove_foreign_key_if_exists :material_transaction_items, column: :supplier_id
    remove_foreign_key_if_exists :material_transaction_items, column: :material_id
    remove_foreign_key_if_exists :orders, column: :user_id
    remove_foreign_key_if_exists :order_items, column: :order_id
    remove_foreign_key_if_exists :order_item_sizes, column: :order_item_id
    remove_foreign_key_if_exists :order_item_sizes, column: :producyt_size_id
    remove_foreign_key_if_exists :personnels, column: :user_id
    remove_foreign_key_if_exists :product_materials, column: :product_id
  end
end
