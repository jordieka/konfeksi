class CreateOrderItemSizes < ActiveRecord::Migration[5.2]
  def change
    create_table :order_item_sizes do |t|
      t.references   :order_item
      t.references   :product_size
      t.integer		 :quantity

      t.timestamps
    end
  end
end
