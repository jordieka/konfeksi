class CreateProductProcurements < ActiveRecord::Migration[5.2]
  def change
    create_table :product_procurements do |t|
      t.datetime      :request_date, default: -> { 'CURRENT_TIMESTAMP' }
      t.references    :users, index: true
      t.integer       :status
      t.integer       :row_status, default: 1

      t.timestamps
    end
  end
end
