class CreateMaterials < ActiveRecord::Migration[5.2]
  def change
    create_table :materials do |t|
      t.string      :name
      t.integer     :stock, default: 0
      t.integer     :type
      t.integer     :category
      t.integer     :row_status, default: 1

      t.timestamps
    end
  end
end
