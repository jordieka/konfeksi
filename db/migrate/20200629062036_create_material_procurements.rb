class CreateMaterialProcurements < ActiveRecord::Migration[5.2]
  def change
    create_table :material_procurements do |t|
      t.datetime      :request_date, default: -> { 'CURRENT_TIMESTAMP' }
      t.references    :user, index: true
      t.integer       :status
      t.integer       :row_status, default: 1
      t.boolean		  :is_change
      t.integer		  :review_by
      
      t.timestamps
    end
  end
end
