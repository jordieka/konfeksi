class ChangeInventoryField < ActiveRecord::Migration[5.2]
  def up
  	remove_column :material_transactions, :supplier_id
  	add_reference :material_transactions, :material_request, index: true
  	add_reference :material_transaction_items, :supplier, index: true
  end

  def down
  	remove_column :material_transactions, :material_request
  	remove_column :material_transaction_items, :supplier
  end
end
