class CreateCustomers < ActiveRecord::Migration[5.2]
  def change
    create_table :customers do |t|
      t.string      :full_name
      t.string      :company_name
      t.string      :phone_number
      t.integer     :customer_type
      t.integer     :row_status, default: 1

      t.timestamps
    end
  end
end
