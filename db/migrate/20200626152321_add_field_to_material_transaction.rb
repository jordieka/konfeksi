class AddFieldToMaterialTransaction < ActiveRecord::Migration[5.2]
  def change
    add_column :material_transactions, :bill_image, :text
  end
end
