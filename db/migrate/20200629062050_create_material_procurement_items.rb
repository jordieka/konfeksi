class CreateMaterialProcurementItems < ActiveRecord::Migration[5.2]
  def change
    create_table :material_procurement_items do |t|
      t.references      :material_procurement, index:true
      t.references      :material, index:true
      t.integer         :quantity
      t.integer         :row_status, default: 1

      t.timestamps
    end
  end
end
