class CreateReports < ActiveRecord::Migration[5.2]
  def change
    create_table :reports do |t|
      t.references 		:user, index: true
      t.integer			:report_type
      t.text			:report

      t.timestamps
    end
  end
end
