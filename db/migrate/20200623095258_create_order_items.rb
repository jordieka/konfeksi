class CreateOrderItems < ActiveRecord::Migration[5.2]
  def change
    create_table :order_items do |t|
      t.references      :orders, index: true
      t.references      :product_types, index: true
      t.integer         :quantity
      t.integer         :production_status
      t.string          :design_image
      t.string          :pattern_image
      t.boolean         :is_sablon
      t.string          :canvass_image
      t.float           :price_per_item
      t.float           :sub_total
      t.integer         :row_status

      t.timestamps
    end
  end
end
