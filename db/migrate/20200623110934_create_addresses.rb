class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.integer       :refer_id
      t.integer       :type
      t.text          :full_address
      t.integer       :row_status, default: 1

      t.timestamps
    end
  end
end
