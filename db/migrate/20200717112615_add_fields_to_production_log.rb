class AddFieldsToProductionLog < ActiveRecord::Migration[5.2]
  def change
    add_column 		:production_logs, :quantity_left, :integer
    add_reference 	:production_logs, :order_item_size, index: true
    
    remove_column 	:production_logs, :order_items_id
  end
end
