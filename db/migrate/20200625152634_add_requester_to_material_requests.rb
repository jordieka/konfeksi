class AddRequesterToMaterialRequests < ActiveRecord::Migration[5.2]
  def change
    add_column :material_requests, :review_by, :int
  end
end
