class CreateProductionOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :production_orders do |t|
      t.references    :order
      t.integer       :status
      t.integer       :order
      t.integer       :row_status, default: 1

      t.timestamps
    end
  end
end
