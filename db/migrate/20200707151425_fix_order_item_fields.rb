class FixOrderItemFields < ActiveRecord::Migration[5.2]
  def change
  	remove_reference :order_items, :product_size, index: true

  	remove_column :order_items, :price_per_item

  	add_column :order_item_sizes, :price_per_item, :float
  	add_column :order_item_sizes, :row_status, :integer, default: 1
  	add_column :order_item_sizes, :sub_total, :float
  end
end
