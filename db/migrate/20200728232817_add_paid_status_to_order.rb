class AddPaidStatusToOrder < ActiveRecord::Migration[5.2]
  def change
  	add_column :orders, :payment_status, :integer, default: 0
  end
end
