class CreateProductSizes < ActiveRecord::Migration[5.2]
  def change
    create_table :product_sizes do |t|
      t.integer :size
      t.float :margin_price
      t.references :product, foreign_key: true, index: true

      t.timestamps
    end
  end
end
