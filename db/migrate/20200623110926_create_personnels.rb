class CreatePersonnels < ActiveRecord::Migration[5.2]
  def change
    create_table :personnels do |t|
      t.references  :users, index: true
      t.string      :full_name
      t.string      :phone_number
      t.datetime    :birth_date
      t.integer     :gender
      t.integer     :position
      t.integer     :row_status, default: 1

      t.timestamps
    end
  end
end
