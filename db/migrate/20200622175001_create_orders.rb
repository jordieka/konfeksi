class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string      :invoice, null: false, unique: true
      t.references  :customer, index: true
      t.integer     :order_status
      t.integer     :total_qnt
      t.float       :down_payment
      t.float       :total
      t.integer     :row_status, default: 1

      t.timestamps
    end
  end
end
