class RemoveColumnCodeFromSupplier < ActiveRecord::Migration[5.2]
  def change
    remove_column :suppliers, :code
  end
end
