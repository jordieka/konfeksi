class CreateMaterialRequestItems < ActiveRecord::Migration[5.2]
  def change
    create_table :material_request_items do |t|
      t.references    :material_request, index: true
      t.references    :material, index: true
      t.integer       :quantity
      t.integer       :row_status, default: 1

      t.timestamps
    end
  end
end
