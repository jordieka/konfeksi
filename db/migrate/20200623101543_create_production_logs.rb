class CreateProductionLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :production_logs do |t|
      t.references    :order_items, index: true
      t.integer       :progress_status
      t.integer       :quantity_finish
      t.text          :description
      t.integer       :row_status, default: 1

      t.timestamps
    end
  end
end
