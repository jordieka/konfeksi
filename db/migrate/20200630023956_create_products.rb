class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name
      t.references :category, foreign_key: true, index: true
      t.integer :gender
      t.float :base_price
      t.float :margin_price
      t.integer :row_status

      t.timestamps
    end
  end
end
