class ChangeUserReferencesOnMaterialRequest < ActiveRecord::Migration[5.2]
  def change
  	rename_column :material_requests, :users_id, :user_id
  	rename_column :personnels, :users_id, :user_id
  end
end
