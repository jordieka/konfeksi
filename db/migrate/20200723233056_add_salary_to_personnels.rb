class AddSalaryToPersonnels < ActiveRecord::Migration[5.2]
  def change
    add_column :personnels, :salary, :float
    add_column :personnels, :salary_type, :integer
  end
end
