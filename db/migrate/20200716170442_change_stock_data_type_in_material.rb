class ChangeStockDataTypeInMaterial < ActiveRecord::Migration[5.2]
  def change
  	change_column :materials, :stock, :float
  end
end
