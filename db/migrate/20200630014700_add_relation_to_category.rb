class AddRelationToCategory < ActiveRecord::Migration[5.2]
  def change
  	remove_column :materials, :category
  	add_reference :materials, :category, index: true
  end
end
