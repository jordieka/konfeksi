class ChangeDataTypeForImagesInOrderItems < ActiveRecord::Migration[5.2]
  def up
  	change_column :order_items, :pattern_image, :text 
  	change_column :order_items, :sablon_image, :text 
  	change_column :order_items, :bordir_image, :text 
  end

  def down
  	change_column :order_items, :pattern_image, :string 
  	change_column :order_items, :sablon_image, :string
  	change_column :order_items, :bordir_image, :string
  end
end
