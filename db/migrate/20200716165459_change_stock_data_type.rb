class ChangeStockDataType < ActiveRecord::Migration[5.2]
  def change
  	change_column :material_request_items, :quantity, :float
  	change_column :material_request_items, :qnt_left, :float
  	change_column :material_transaction_items, :quantity, :float
  	change_column :material_procurement_items, :quantity, :float
  end
end
