class CreateProductMaterialOptions < ActiveRecord::Migration[5.2]
  def change
    create_table :product_material_options do |t|
      t.references     :product_types, index: true
      t.references     :materials, index: true
      t.integer        :type
      t.float          :material_per_pcs
      t.integer        :unit
      t.float          :material_margin
      t.integer        :row_status, default: 1

      t.timestamps
    end
  end
end
