class CreateMaterialTransactionItems < ActiveRecord::Migration[5.2]
  def change
    create_table :material_transaction_items do |t|
      t.references    :material_transactions, index: true
      t.references    :materials, index: true
      t.integer       :quantity
      t.float         :sub_total
      t.integer       :row_status, default: 1

      t.timestamps
    end
  end
end
