class ChangeFieldsOnMaterialTransaction < ActiveRecord::Migration[5.2]
  def change
  	rename_column :material_transactions, :suppliers_id, :supplier_id
  	remove_column :material_transactions, :transaction_code
  	add_column :material_transactions, :payment_image, :string
  end
end
