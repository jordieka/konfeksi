class CreateSuppliers < ActiveRecord::Migration[5.2]
  def change
    create_table :suppliers do |t|
      t.string      :name
      t.string      :code, null: false, unique: true
      t.string      :phone_number
      t.text        :address
      t.integer     :row_status, default: 1

      t.timestamps
    end
  end
end
