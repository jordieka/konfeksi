class AddEmailToPersonnel < ActiveRecord::Migration[5.2]
  def change
    add_column :personnels, :email, :string
  end
end
