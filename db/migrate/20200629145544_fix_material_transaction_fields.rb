class FixMaterialTransactionFields < ActiveRecord::Migration[5.2]
  def change
	remove_index :material_transactions, name: "index_material_transactions_on_material_request_id"
	remove_column :material_transactions, :material_request_id
	add_reference :material_transactions, :material_procurement, index:true
  end
end
