class NewFieldForOrderItems < ActiveRecord::Migration[5.2]
  def change
  	add_column :order_items, :is_ready, :boolean, default: false
  	add_column :order_items, :is_bordir, :boolean, default: false
  	rename_column :order_items, :design_image, :bordir_image
  	rename_column :order_items, :canvass_image, :sablon_image

  	add_column :order_items, :sablon_price, :float
  	add_column :order_items, :sablon_position, :integer
  	add_column :order_items, :bordir_price, :float
  	add_column :order_items, :bordir_position, :integer
  end
end
