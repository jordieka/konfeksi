class CreateOrderItemMaterials < ActiveRecord::Migration[5.2]
  def change
    create_table :order_item_materials do |t|
      t.references      :materials, index:true
      t.references      :order_items, index:true
      t.integer         :quantity_expected
      t.integer         :quantity_used
      t.float           :base_price
      t.integer         :row_status, default: 1

      t.timestamps
    end
  end
end
