class ChangeFieldsOnReports < ActiveRecord::Migration[5.2]
  def change
    remove_column :reports, :report_type
    add_column :reports, :subject, :string
    add_column :reports, :is_read, :boolean, default: false
    add_column :reports, :read_date, :datetime
    add_column :reports, :row_status, :integer, default: 1
  end
end
