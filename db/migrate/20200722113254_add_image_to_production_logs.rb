class AddImageToProductionLogs < ActiveRecord::Migration[5.2]
  def change
  	add_column :production_logs, :image, :text
  end
end
