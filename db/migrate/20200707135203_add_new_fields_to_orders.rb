class AddNewFieldsToOrders < ActiveRecord::Migration[5.2]
  def change
  	add_column :orders, :created_by, :integer
  	remove_reference :order_items, :orders, index: true
  	remove_reference :order_items, :product_types, index: true

  	add_reference :order_items, :order, index: true
  	add_reference :order_items, :product, index: true
  	add_reference :order_items, :product_size, index: true
  end
end
