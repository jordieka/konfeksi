class CreateAbsents < ActiveRecord::Migration[5.2]
  def change
    create_table :absents do |t|
      t.references :personnel, foreign_key: true
      t.references :user, foreign_key: true
      t.integer    :status, default: 0
      t.text       :description
      t.integer    :extra_hour

      t.timestamps
    end
  end
end
