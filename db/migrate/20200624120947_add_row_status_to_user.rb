class AddRowStatusToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :row_status, :integer, default: 1
    add_column :users, :temp_password, :string
  end
end
