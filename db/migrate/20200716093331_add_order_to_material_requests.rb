class AddOrderToMaterialRequests < ActiveRecord::Migration[5.2]
  def change
  	add_reference :material_requests, :order, index: true
  end
end
