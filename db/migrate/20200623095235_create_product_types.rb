class CreateProductTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :product_types do |t|
      t.string      :name
      t.integer     :type
      t.integer     :gender
      t.float       :base_price
      t.float       :margin_price
      t.integer     :row_status, default: 1

      t.timestamps
    end
  end
end
