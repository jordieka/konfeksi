class AddFieldToPersonnel < ActiveRecord::Migration[5.2]
  def change
    add_column :personnels, :remaining_day_off, :integer, default: 12
  end
end
