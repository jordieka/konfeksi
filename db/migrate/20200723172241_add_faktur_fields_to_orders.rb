class AddFakturFieldsToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :shipping_invoice, :string
    add_column :orders, :shipping_invoice_image, :text
    add_column :orders, :payment_image, :text
    add_column :orders, :kurir, :integer
  end
end
