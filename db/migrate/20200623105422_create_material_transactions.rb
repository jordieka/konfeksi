class CreateMaterialTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :material_transactions do |t|
      t.string      :invoice
      t.string      :transaction_code
      t.datetime    :order_date
      t.datetime    :payment_date
      t.references  :suppliers, index: true
      t.integer     :order_status
      t.datetime    :finish_date
      t.float       :total
      t.integer     :row_status, default: 1

      t.timestamps
    end
  end
end
