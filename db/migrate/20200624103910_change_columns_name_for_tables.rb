class ChangeColumnsNameForTables < ActiveRecord::Migration[5.2]
  def self.up
    rename_column :materials, :type, :material_use
    rename_column :product_types, :type, :category
    rename_column :addresses, :type, :target
  end

  def self.down
    # rename back if you need or do something else or do nothing
  end
end
