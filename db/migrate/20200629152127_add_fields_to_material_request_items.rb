class AddFieldsToMaterialRequestItems < ActiveRecord::Migration[5.2]
  def change
    add_column :material_request_items, :is_complete, :boolean, default: false
    add_column :material_request_items, :qnt_left, :integer
    # add_column :material_request_items, :stock_before, :integer
  end
end
