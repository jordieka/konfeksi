class RenamePaymentImageOnMaterialTransaction < ActiveRecord::Migration[5.2]
  def change
  	change_column :material_transactions, :payment_image, :text
  end
end
