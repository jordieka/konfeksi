class AddFieldToMaterial < ActiveRecord::Migration[5.2]
  def change
    add_column :materials, :common_price, :float
    add_column :materials, :unit, :integer
  end
end
