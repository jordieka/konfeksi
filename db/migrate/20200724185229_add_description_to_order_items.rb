class AddDescriptionToOrderItems < ActiveRecord::Migration[5.2]
  def change
    add_column :order_items, :pattern_description, :text
  end
end
