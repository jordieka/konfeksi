class CreateMaterialRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :material_requests do |t|
      t.datetime      :request_date, default: -> { 'CURRENT_TIMESTAMP' }
      t.references    :users, index: true
      t.integer       :status
      t.boolean       :is_change
      t.integer       :row_status, default: 1

      t.timestamps
    end
  end
end
