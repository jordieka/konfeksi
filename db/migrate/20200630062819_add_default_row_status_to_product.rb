class AddDefaultRowStatusToProduct < ActiveRecord::Migration[5.2]
  	def up
    	change_column_default :products, :row_status, 1
    	add_column :product_sizes, :row_status, :integer, default: 1
	end

	def down
		change_column_default :products, :row_status, nil
		remove_column :product_sizes, :row_status
	end
end
