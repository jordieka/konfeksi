class UpdateMaterialTransactionsField < ActiveRecord::Migration[5.2]
  def change
  	rename_column :material_transaction_items, :materials_id, :material_id
  	rename_column :material_transaction_items, :material_transactions_id, :material_transaction_id
  end
end
