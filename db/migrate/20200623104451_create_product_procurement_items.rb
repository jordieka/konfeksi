class CreateProductProcurementItems < ActiveRecord::Migration[5.2]
  def change
    create_table :product_procurement_items do |t|
      t.references      :product_procurements, index:true
      t.references      :materials, index:true
      t.integer         :quantity
      t.integer         :row_status, default: 1

      t.timestamps
    end
  end
end
