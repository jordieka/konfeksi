class AddFkToTables < ActiveRecord::Migration[5.2]
  DOWNTIME = false

  def up
    # safe to use: it requires short lock on the table since we don't validate the foreign key
    add_foreign_key :materials, :categories, on_delete: :cascade, validate: false
    add_foreign_key :products, :categories, on_delete: :cascade, validate: false
  end

  def down
    remove_foreign_key_if_exists :materials, column: :category_id
    remove_foreign_key_if_exists :products, column: :category_id
  end
end
