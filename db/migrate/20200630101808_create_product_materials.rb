class CreateProductMaterials < ActiveRecord::Migration[5.2]
  def change
    create_table :product_materials do |t|
      t.references :product, indek: true
      t.references :material, index: true
      t.float	   :use_per_unit
      t.integer	   :row_status

      t.timestamps
    end
  end
end
