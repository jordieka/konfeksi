class AddFieldsToCustomer < ActiveRecord::Migration[5.2]
  def change
    add_column :customers, :id_card, :string
    add_column :customers, :email, :string
    add_column :customers, :company_phone, :string
  end
end
