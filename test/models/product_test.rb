# == Schema Information
#
# Table name: products
#
#  id            :bigint           not null, primary key
#  base_price    :float
#  gender        :integer
#  margin_price  :float
#  name          :string
#  pattern_image :text
#  row_status    :integer          default(1)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  category_id   :bigint
#  user_id       :bigint
#
# Indexes
#
#  index_products_on_category_id  (category_id)
#  index_products_on_user_id      (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id) ON DELETE => cascade
#  fk_rails_...  (user_id => users.id)
#
require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
