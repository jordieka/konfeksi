# == Schema Information
#
# Table name: material_procurement_items
#
#  id                      :bigint           not null, primary key
#  quantity                :float
#  row_status              :integer          default(1)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  material_id             :bigint
#  material_procurement_id :bigint
#
# Indexes
#
#  index_material_procurement_items_on_material_id              (material_id)
#  index_material_procurement_items_on_material_procurement_id  (material_procurement_id)
#
# Foreign Keys
#
#  fk_rails_...  (material_id => materials.id) ON DELETE => cascade
#  fk_rails_...  (material_procurement_id => material_procurements.id) ON DELETE => cascade
#
require 'test_helper'

class MaterialProcurementItemTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
