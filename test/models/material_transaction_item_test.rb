# == Schema Information
#
# Table name: material_transaction_items
#
#  id                      :bigint           not null, primary key
#  quantity                :float
#  row_status              :integer          default(1)
#  sub_total               :float
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  material_id             :bigint
#  material_transaction_id :bigint
#  supplier_id             :bigint
#
# Indexes
#
#  index_material_transaction_items_on_material_id              (material_id)
#  index_material_transaction_items_on_material_transaction_id  (material_transaction_id)
#  index_material_transaction_items_on_supplier_id              (supplier_id)
#
# Foreign Keys
#
#  fk_rails_...  (material_id => materials.id) ON DELETE => cascade
#  fk_rails_...  (material_transaction_id => material_transactions.id) ON DELETE => cascade
#  fk_rails_...  (supplier_id => suppliers.id) ON DELETE => cascade
#

require 'test_helper'

class MaterialTransactionItemTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
