# == Schema Information
#
# Table name: material_request_items
#
#  id                   :bigint           not null, primary key
#  is_complete          :boolean          default(FALSE)
#  qnt_left             :float
#  quantity             :float
#  row_status           :integer          default(1)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  material_id          :bigint
#  material_requests_id :bigint
#
# Indexes
#
#  index_material_request_items_on_material_id           (material_id)
#  index_material_request_items_on_material_requests_id  (material_requests_id)
#
# Foreign Keys
#
#  fk_rails_...  (material_id => materials.id) ON DELETE => cascade
#  fk_rails_...  (material_requests_id => material_requests.id) ON DELETE => cascade
#

require 'test_helper'

class MaterialRequestItemTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
