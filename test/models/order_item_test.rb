# == Schema Information
#
# Table name: order_items
#
#  id                  :bigint           not null, primary key
#  bordir_image        :text
#  bordir_position     :integer
#  bordir_price        :float
#  is_bordir           :boolean          default(FALSE)
#  is_ready            :boolean          default(FALSE)
#  is_sablon           :boolean
#  pattern_description :text
#  pattern_image       :text
#  production_status   :integer
#  quantity            :integer
#  row_status          :integer
#  sablon_image        :text
#  sablon_position     :integer
#  sablon_price        :float
#  sub_total           :float
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  order_id            :bigint
#  product_id          :bigint
#
# Indexes
#
#  index_order_items_on_order_id    (order_id)
#  index_order_items_on_product_id  (product_id)
#
# Foreign Keys
#
#  fk_rails_...  (order_id => orders.id) ON DELETE => cascade
#  fk_rails_...  (product_id => products.id) ON DELETE => cascade
#

require 'test_helper'

class OrderItemTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
