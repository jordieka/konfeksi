# == Schema Information
#
# Table name: material_transactions
#
#  id                      :bigint           not null, primary key
#  bill_image              :text
#  finish_date             :datetime
#  invoice                 :string
#  order_date              :datetime
#  order_status            :integer
#  payment_date            :datetime
#  payment_image           :text
#  row_status              :integer          default(1)
#  total                   :float
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  material_procurement_id :bigint
#
# Indexes
#
#  index_material_transactions_on_material_procurement_id  (material_procurement_id)
#
# Foreign Keys
#
#  fk_rails_...  (material_procurement_id => material_procurements.id) ON DELETE => cascade
#

require 'test_helper'

class MaterialTransactionTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
