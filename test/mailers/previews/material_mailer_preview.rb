# Preview all emails at http://localhost:3000/rails/mailers/material_mailer
class MaterialMailerPreview < ActionMailer::Preview

	def request_material
		MaterialMailer.request_material(MaterialRequest.last)
	end

	def update_request_material
		MaterialMailer.update_request_material(MaterialRequest.last)
	end

end
