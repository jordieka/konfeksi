# Preview all emails at http://localhost:3000/rails/mailers/order_mailer
class OrderMailerPreview < ActionMailer::Preview

	def send_order_ready
		OrderMailer.send_order_ready(Order.last, User.last)
	end
end
